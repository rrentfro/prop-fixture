const Commands = require('../../lib/commands');
const Arg = require('../../lib/arg');
const PropertyGenerator = require('../generator/PropertyGenerator');

module.exports = () => {
  const command = Arg();
  const res = [];
  const t = command.command3 && parseFloat(command.command3) > 0 ? command.command3 : 1;
  if (command.arg) {
    switch (command.command2.toLowerCase()) {
      default:
        Commands.InvalidCommand();
        break;
      case 'properties':
        for (let i = 0; i < t; i += 1) {
          // Deep Copy ...
          const p = JSON.parse(JSON.stringify(PropertyGenerator.GenerateProperty()));
          res.push(p);
        }
        console.log(JSON.stringify(res, null, 2));
    }
  }
};
