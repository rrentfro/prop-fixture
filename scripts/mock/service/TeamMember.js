module.exports = {
  getAPI() {
    this.routes = [
      {
        method: 'get',
        path: '/v1/team-member/',
        action: (req, res) => {
          res.json({ title: 'Get a specific team-member' });
        },
      },
      {
        method: 'post',
        path: '/v1/team-member/',
        action: (req, res) => {
          res.json({ title: 'Add a team-member' });
        },
      },
      {
        method: 'patch',
        path: '/v1/team-member/',
        action: (req, res) => {
          res.json({ title: 'Update a team-member' });
        },
      },
      {
        method: 'delete',
        path: '/v1/team-member/',
        action: (req, res) => {
          res.json({ title: 'Delete a team-member' });
        },
      },
    ];
    return {
      title: 'team-member',
      routes: this.routes,
    };
  },
};
