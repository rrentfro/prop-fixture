const account = require('./account/account');
const agent = require('./agents/agents');
const analytics = require('./analytics/analytics');
const awards = require('./awards/awards');
const blog = require('./blog/blog');
const calendar = require('./calendar/calendar');
const career = require('./career/career');
const communication = require('./communication/communication');
const companies = require('./companies/companies');
const content = require('./content/content');
const currency = require('./currency/currency');
const events = require('./events/events');
const favorite = require('./favorite/favorite');
const lead = require('./lead/lead');
const location = require('./location/location');
const media = require('./media/media');
const offices = require('./offices/offices');
const people = require('./people/people');
const property = require('./property/property');
const search = require('./search/search');
const team = require('./teams/teams');
const company = require('./company/company');
const office = require('./office/office');
const staff = require('./staff/staff');

module.exports = {
  account,
  agent,
  analytics,
  awards,
  blog,
  calendar,
  career,
  communication,
  companies,
  content,
  currency,
  events,
  favorite,
  lead,
  location,
  media,
  offices,
  people,
  property,
  search,
  team,
  company,
  office,
  staff,
};
