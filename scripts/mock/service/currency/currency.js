const UUID = require('uuid4');
const faker = require('faker');
const lMerge = require('lodash/merge');
const log = require('../../lib/log');
const ErrorDTO = require('../../../../dto/common/error');
const ErrorItem = require('../../../../dto/common/ErrorItem');
const CurrencyConstants = require('../../../../clients/currency/CurrencyConstants');
const ExchangeRateDTO = require('../../../../dto/currency/ExchangeRateDTO');
const DTOFactory = require('../../../../lib/factory/dto');

module.exports = {
  getAPI() {
    this.routes = [
      {
        method: 'get',
        path: '/v1/currency/convert/:source/:TargetArray',
        action: (req, res) => {
          if (req.params && req.params.source && req.params.TargetArray) {
            log.LogRequest(req.params, req.originalUrl);

            const Targets = req.params.TargetArray.split(',');
            const quotes = {};
            const e = {
              error: false,
              errors: [],
            };

            Targets.forEach((i) => {
              const isValid = Object.prototype.hasOwnProperty.call(
                CurrencyConstants.CURRENCY_CODE,
                i,
              );

              if (isValid) {
                quotes[i] = faker.datatype.float({
                  min: 0.1,
                  max: 2,
                });
              } else {
                e.error = true;
                e.errors.push(`INVALID_CURRENCY__${i}`);
              }
            });

            const DTO = DTOFactory.Combine(ExchangeRateDTO);
            if (e.error) {
              const error = DTOFactory.Combine(ErrorDTO);
              error.message = 'An error has occurred during currency quoting.';
              error.code = 404;
              e.errors.forEach((reason) => {
                const NewError = lMerge(ErrorItem, {
                  domain: 'MOCK',
                  reason,
                  message: '',
                  location: 'currency:convert',
                  locationType: 'API',
                  extendedHelp: '',
                  sendReport: 'https://support.example.com',
                });

                error.errors.push(NewError);
              });

              res.status(400).json(error);
            } else {
              DTO.trace = UUID();
              DTO.data.kind = 'ExchangeRate';
              DTO.data.source = 'USD';
              DTO.data.updated = Math.round(Date.now() / 1000);
              DTO.data.quotes = quotes;

              log.LogResponse(DTO, req.originalUrl);
              res.status(e.error === false ? 200 : 400).json(DTO);
            }
          } else {
            res.status(400).json({
              error: true,
              errors: ['INVALID_PARAMETERS'],
            });
          }
        },
      },
      {
        method: 'get',
        path: '/v1/currency/quote/:target',
        action: (req, res) => {
          if (
            req.params &&
            req.params.target &&
            Object.prototype.hasOwnProperty.call(CurrencyConstants.CURRENCY_CODE, req.params.target)
            // eslint-disable-next-line no-empty
          ) {
            log.LogRequest(req.params, req.originalUrl);

            const DTO = DTOFactory.Combine(ExchangeRateDTO);
            DTO.trace = UUID();
            DTO.data.kind = 'ExchangeRate';
            DTO.data.source = 'USD';
            DTO.data.updated = Math.round(Date.now() / 1000);
            DTO.data.quotes[req.params.target] = faker.datatype.float({
              min: 0.1,
              max: 2,
            });

            log.LogResponse(DTO, req.originalUrl);
            res.status(200).json(DTO);
          } else {
            const error = DTOFactory.Combine(ErrorDTO);
            error.message = 'An error has occurred during currency quoting.';
            error.code = 404;
            const NewError = lMerge(ErrorItem, {
              domain: 'MOCK',
              reason: 'The requested symbol was invalid',
              message: 'Invalid currency requested',
              location: 'currency:quote',
              locationType: 'API',
              extendedHelp: '',
              sendReport: 'https://support.example.com',
            });
            error.errors.push(NewError);
            res.status(400).json(error);
          }
        },
      },
    ];
    return {
      title: 'Currency',
      routes: this.routes,
    };
  },
};
