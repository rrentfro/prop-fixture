const lMerge = require('lodash/merge');
const faker = require('faker');
const DTOFactory = require('../../../../lib/factory/dto');
const ErrorDTO = require('../../../../dto/common/error');
const ErrorItem = require('../../../../dto/common/ErrorItem');
const OfficeGenerator = require('../../../generator/officeGenerator');

const log = require('../../lib/log');

module.exports = {
  getAPI() {
    this.routes = [
      {
        method: 'get',
        path: '/v1/office/:term',
        action: (req, res) => {
          // Search validator
          const isValidSearch = true;

          if (isValidSearch) {
            log.LogRequest(req.params, req.originalUrl);
            const DTO = OfficeGenerator.GenerateOffice();
            faker.seed(Math.floor(Math.random() * 64000) + 1000);
            log.LogResponse(DTO, req.originalUrl);
            res.status(200).json(DTO);
          } else {
            const error = DTOFactory.Combine(ErrorDTO);
            error.message = 'An error has occurred during currency quoting.';
            error.code = 404;
            const NewError = lMerge(ErrorItem, {
              domain: 'MOCK',
              reason: 'The requested symbol was invalid',
              message: 'Invalid currency requested',
              location: 'currency:quote',
              locationType: 'API',
              extendedHelp: '',
              sendReport: 'https://support.example.com',
            });
            error.errors.push(NewError);
            res.status(400).json(error);
          }
        },
      },
    ];
    return {
      title: 'Office',
      routes: this.routes,
    };
  },
};
