const lMerge = require('lodash/merge');
const DTOFactory = require('../../../../lib/factory/dto');
const ErrorDTO = require('../../../../dto/common/error');
const ErrorItem = require('../../../../dto/common/ErrorItem');
const EventsGenerator = require('../../../generator/EventsGenerator');
const log = require('../../lib/log');

module.exports = {
  getAPI() {
    this.routes = [
      {
        method: 'get',
        path: '/v1/events/:AwardID?',
        action: (req, res) => {
          let isValidSearch = false;
          if (req.params.AwardID) {
            isValidSearch = true;
          }
          if (isValidSearch) {
            log.LogRequest(req.query, req.originalUrl);
            const DTO = EventsGenerator.GenerateListOfEvents();
            log.LogResponse(DTO, req.originalUrl);
            res.status(200).json(DTO);
          } else {
            const error = DTOFactory.Combine(ErrorDTO);
            error.message = 'An error has occurred during get events.';
            error.code = 404;
            const NewError = lMerge(ErrorItem, {
              domain: 'MOCK',
              reason: 'The requested symbol was invalid',
              message: 'Invalid events data requested',
              location: 'events:award',
              locationType: 'API',
              extendedHelp: '',
              sendReport: 'https://support.example.com',
            });
            error.errors.push(NewError);
            res.status(400).json(error);
          }
        },
      },
      {
        method: 'get',
        path: '/v1/events/:AwardID?/:AgentID?',
        action: (req, res) => {
          let isValidSearch = false;
          if (req.params.AwardID && req.params.AgentID) {
            isValidSearch = true;
          }
          if (isValidSearch) {
            log.LogRequest(req.query, req.originalUrl);
            const DTO = EventsGenerator.GenerateListOfEventsByAgent();
            log.LogResponse(DTO, req.originalUrl);
            res.status(200).json(DTO);
          } else {
            const error = DTOFactory.Combine(ErrorDTO);
            error.message = 'An error has occurred during get events by agent.';
            error.code = 404;
            const NewError = lMerge(ErrorItem, {
              domain: 'MOCK',
              reason: 'The requested symbol was invalid',
              message: 'Invalid events by agent data requested',
              location: 'eventsbyagent:award',
              locationType: 'API',
              extendedHelp: '',
              sendReport: 'https://support.example.com',
            });
            error.errors.push(NewError);
            res.status(400).json(error);
          }
        },
      },
    ];
    return {
      title: 'Events',
      routes: this.routes,
    };
  },
};
