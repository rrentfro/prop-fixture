const lMerge = require('lodash/merge');
const DTOFactory = require('../../../../lib/factory/dto');
const ErrorDTO = require('../../../../dto/common/error');
const ErrorItem = require('../../../../dto/common/ErrorItem');
const AwardsGenerator = require('../../../generator/AwardsGenerator');
const log = require('../../lib/log');

module.exports = {
  getAPI() {
    this.routes = [
      {
        method: 'get',
        path: '/v1/award/:AwardID?',
        action: (req, res) => {
          let isValidSearch = false;
          if (req.params.AwardID) {
            isValidSearch = true;
          }
          if (isValidSearch) {
            log.LogRequest(req.query, req.originalUrl);
            const DTO = AwardsGenerator.GenerateGrantsAwards();
            log.LogResponse(DTO, req.originalUrl);
            res.status(200).json(DTO);
          } else {
            const error = DTOFactory.Combine(ErrorDTO);
            error.message = 'An error has occurred during retrieving grants for award.';
            error.code = 404;
            const NewError = lMerge(ErrorItem, {
              domain: 'MOCK',
              reason: 'The requested symbol was invalid',
              message: 'Invalid grants for award data requested',
              location: 'grants:award',
              locationType: 'API',
              extendedHelp: '',
              sendReport: 'https://support.example.com',
            });
            error.errors.push(NewError);
            res.status(400).json(error);
          }
        },
      },
      {
        method: 'get',
        path: '/v1/progress/:AwardID?',
        action: (req, res) => {
          let isValidSearch = false;
          if (req.params.AwardID) {
            isValidSearch = true;
          }
          if (isValidSearch) {
            log.LogRequest(req.query, req.originalUrl);
            const DTO = AwardsGenerator.GenerateProgressAwards();
            log.LogResponse(DTO, req.originalUrl);
            res.status(200).json(DTO);
          } else {
            const error = DTOFactory.Combine(ErrorDTO);
            error.message = 'An error has occurred during retrieving progress awards.';
            error.code = 404;
            const NewError = lMerge(ErrorItem, {
              domain: 'MOCK',
              reason: 'The requested symbol was invalid',
              message: 'Invalid progress awards data requested',
              location: 'progress:awards',
              locationType: 'API',
              extendedHelp: '',
              sendReport: 'https://support.example.com',
            });
            error.errors.push(NewError);
            res.status(400).json(error);
          }
        },
      },
    ];
    return {
      title: 'Awards',
      routes: this.routes,
    };
  },
};
