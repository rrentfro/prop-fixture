const lMerge = require('lodash/merge');
const DTOFactory = require('../../../../lib/factory/dto');
const ErrorDTO = require('../../../../dto/common/error');
const ErrorItem = require('../../../../dto/common/ErrorItem');
const CompanyGenerator = require('../../../generator/CompaniesGenerator');
const log = require('../../lib/log');

module.exports = {
  getAPI() {
    this.routes = [
      {
        method: 'get',
        path: '/v1/companies',
        action: (req, res) => {
          let isValidSearch = false;
          if (req.query.asOf) {
            isValidSearch = true;
          }
          if (isValidSearch) {
            log.LogRequest(req.query, req.originalUrl);
            const DTO = CompanyGenerator.GenerateCompany();
            log.LogResponse(DTO, req.originalUrl);
            res.status(200).json(DTO);
          } else {
            const error = DTOFactory.Combine(ErrorDTO);
            error.message = 'An error has occurred during get company awards.';
            error.code = 404;
            const NewError = lMerge(ErrorItem, {
              domain: 'MOCK',
              reason: 'The requested symbol was invalid',
              message: 'Invalid company award data requested',
              location: 'companyaward:quote',
              locationType: 'API',
              extendedHelp: '',
              sendReport: 'https://support.example.com',
            });
            error.errors.push(NewError);
            res.status(400).json(error);
          }
        },
      },
    ];
    return {
      title: 'Company',
      routes: this.routes,
    };
  },
};
