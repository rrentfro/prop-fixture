const PeopleGenerator = require('../../../generator/PeopleGenerator');
const log = require('../../lib/log');

module.exports = {
  getAPI() {
    this.routes = [
      {
        method: 'get',
        path: '/v1/mdmpeople/people/:masterId',
        action: (req, res) => {
          if (req.params && req.params.masterId) {
            log.LogRequest(req.params, req.originalUrl);
            const DTO = PeopleGenerator.GeneratePeopleListing();
            log.LogResponse(DTO, req.originalUrl);
            res.status(200).json(DTO);
          } else {
            res.status(400).json({
              error: true,
              errors: ['INVALID_PARAMETERS'],
            });
          }
        },
      },
      {
        method: 'get',
        path: '/v1/mdmpeople/people',
        action: (req, res) => {
          if (req.query) {
            log.LogRequest(req.query, req.originalUrl);
            const DTO = PeopleGenerator.GeneratePeopleSearch();
            log.LogResponse(DTO, req.originalUrl);
            res.status(200).json(DTO);
          } else {
            res.status(400).json({
              error: true,
              errors: ['INVALID_PARAMETERS'],
            });
          }
        },
      },
      {
        method: 'get',
        path: '/v1/mdmpeople/people/okta/:oktaId',
        action: (req, res) => {
          if (req.query) {
            log.LogRequest(req.params, req.originalUrl);
            const DTO = PeopleGenerator.GeneratePeopleSearchByOkta();
            log.LogResponse(DTO, req.originalUrl);
            res.status(200).json(DTO);
          } else {
            res.status(400).json({
              error: true,
              errors: ['INVALID_PARAMETERS'],
            });
          }
        },
      },
    ];
    return {
      title: 'Favorite',
      routes: this.routes,
    };
  },
};
