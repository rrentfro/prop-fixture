const ENV = require('dotenv').config();
const debug = require('debug')('express-test:server');
const http = require('http');
const server = require('./server');

const ENV_PORT = process.env.MOCK_PORT || ENV.parsed.MOCK_PORT || '2099';

module.exports = {
  NewMock() {
    const NewServer = server.NewServer();
    NewServer.set('ENV_PORT', ENV_PORT);
    const api = http.createServer(NewServer);
    api.listen(ENV_PORT);

    api.on('error', (e) => {
      if (e.syscall !== 'listen') {
        throw e;
      }

      const bind = typeof ENV_PORT === 'string' ? `Pipe ${ENV_PORT}` : `Port ${ENV_PORT}`;
      switch (e.code) {
        case 'EACCES':
          console.error(`${bind} requires elevated privileges`);
          process.exit(1);
          break;
        case 'EADDRINUSE':
          console.error(`${bind} is already in use`);
          process.exit(1);
          break;
        default:
          throw e;
      }
    });

    api.on('listening', () => {
      const address = api.address();
      const bind = typeof address === 'string' ? `pipe ${address}` : `ENV_PORT ${address.port}`;
      debug(`Listening on ${bind}`);
    });
  },
};
