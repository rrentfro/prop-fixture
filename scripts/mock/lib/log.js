const ENV = require('dotenv').config();
const chalk = require('chalk');
const ascii = require('../../../lib/constant/ascii');

const LOG_HTTP_REQUEST =
  (ENV.parsed.LOG_HTTP_REQUEST && ENV.parsed.LOG_HTTP_REQUEST.toLowerCase() !== 'false') || false;
const LOG_HTTP_RESPONSE =
  (ENV.parsed.LOG_HTTP_RESPONSE && ENV.parsed.LOG_HTTP_RESPONSE.toLowerCase() !== 'false') || false;

module.exports = {
  LogRequest(params, path) {
    if (LOG_HTTP_REQUEST) {
      console.log(chalk.blueBright(ascii.line2));
      console.log(`HTTP Request - Source: ${path}`);
      console.log(chalk.blueBright(ascii.line2));
      console.log(JSON.stringify(params, null, 2));
      console.log(chalk.blueBright(ascii.line));
    }
  },
  LogResponse(DTO, path) {
    if (LOG_HTTP_RESPONSE) {
      console.log(chalk.greenBright(ascii.line2));
      console.log(`HTTP Response - Source: ${path}`);
      console.log(chalk.greenBright(ascii.line2));
      console.log(JSON.stringify(DTO, null, 2));
      console.log(chalk.greenBright(ascii.line));
    }
  },
};
