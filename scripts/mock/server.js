const ENV = require('dotenv').config();
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const express = require('express');

const server = express();
const chalk = require('chalk');
const APIRoutes = require('./routes');
const ascii = require('../../lib/constant/ascii');

const ENV_PORT = process.env.MOCK_PORT || ENV.parsed.MOCK_PORT || '2099';

// server.router
module.exports = {
  NewServer() {
    console.log(chalk.greenBright(`Mock Server - Running - Port: ${ENV_PORT}`));
    console.log(chalk.greenBright(ascii.line2));

    server.use(logger('dev'));
    server.use(express.json());
    server.use(express.urlencoded({ extended: false }));
    server.use(cookieParser());
    // server.use(express.static(path.join(__dirname, 'public')));

    console.log(chalk.yellowBright('Mounting Services'));
    console.log(chalk.greenBright(ascii.line2));

    // eslint-disable-next-line no-restricted-syntax
    for (const [, value] of Object.entries(APIRoutes)) {
      const service = value.getAPI();
      const { routes } = service;
      console.log(`Adding Service: ${service.title}`);
      for (let i = 0; i < routes.length; i += 1) {
        server[routes[i].method](routes[i].path, routes[i].action);
        console.log(
          `SERVICE: ${service.title} METHOD: ${routes[i].method} PATH: ${routes[i].path}`,
        );
      }
    }
    return server;
  },
};
