const UUID = require('uuid4');
const faker = require('faker');
const lClone = require('lodash/clone');
const lSample = require('lodash/sample');
const lMerge = require('lodash/merge');
const PeopleDTO = require('../../../dto/people/PeopleDTO');
const SourceMetaDataListDTO = require('../../../dto/people/SourceMetaDataListDTO');
const PhonenumbersDTO = require('../../../dto/people/PhoneNumbersDTO');
const EmailAccountsDTO = require('../../../dto/people/EmailAccounts');
const SearchDTO = require('../../../dto/people/SearchDTO');
const StaffDTO = require('../../../dto/people/StaffDTO');
const BasicInfoDTO = require('../../../dto/people/BasicPersonInfoDTO');
const WebOptionsDTO = require('../../../dto/people/WebOptionsDTO');
const CompanyOfficeAssociationsDTO = require('../../../dto/people/CompanyOfficeAssociationDTO');
const OfficeDTO = require('../../../dto/people/OfficeDTO');
const JobTitlesDTO = require('../../../dto/people/JobTitlesDTO');
const MediaDTO = require('../../../dto/people/MediaDTO');
const PeopleByOktaDTO = require('../../../dto/people/PeopleByOktaDTO');
const MasterEntitiesDTO = require('../../../dto/people/MasterEntities');
const AgentDTO = require('../../../dto/people/AgentDTO');
const FaxNumbersDTO = require('../../../dto/people/FaxDTO');
const AddressesDTO = require('../../../dto/people/AddressesDTO');
const OriginalAddressDTO = require('../../../dto/people/OriginalAddressDTO');
const EmergencyDTO = require('../../../dto/people/EmergencyDTO');
const SocialMediaAccountsDTO = require('../../../dto/people/SocialMediaAccountsDTO');
const WrittenLanguagesDTO = require('../../../dto/people/WrittenLanguagesDTO');
const SpokenLanguagesDTO = require('../../../dto/people/SpokenLanguagesDTO');
const LanguagesDTO = require('../../../dto/people/LanguagesDTO');
const LearningPlansDTO = require('../../../dto/people/LearningPlansDTO');
const MlsMembershipsDTO = require('../../../dto/people/MlsMembershipsDTO');
const InsurancesDTO = require('../../../dto/people/InsurancesDTO');
const OtherMembershipsDTO = require('../../../dto/people/OtherMemberships');
const CustomAttributesDTO = require('../../../dto/people/CustomAttributesDTO');
const AgentRiskProfileDTO = require('../../../dto/people/AgentRiskProfileDTO');
const AwardsDTO = require('../../../dto/people/AwardsDTO');
const DesignationDTO = require('../../../dto/people/DesignationDTO');
const AgentPerformanceDTO = require('../../../dto/people/AgentPerformanceDTO');

const GeneratorOptions = {
  sourceSystemName: ['', 'MDM', 'Dash', 'RLG_DASH'],
  sourceEntityName: ['Person', 'PersonGUID'],
  sourceSystemCode: ['', 'RLG_DASH'],
  dataAvailabilityStatus: ['A', 'P'],
  objVersion: [0, 1, 2, 3, 4],
  apiVersion: ['1.0.0', '2.0.0'],
  namePrefix: ['MR', 'Mrs'],
  maritalStatus: ['Married', 'Single'],
  phoneType: ['BusinessPhone', 'PersonalPhone'],
  emailType: ['', 'BusinessEmail'],
  brandName: ['', 'ERA'],
  brandCode: ['', 'ERA'],
  mediaType: ['', 'Image'],
  mediaUsageType: ['', 'CompanyLogo'],
  addressType: ['Current', 'Permanent'],
  relation: ['Husband', 'Wife', 'Father', 'Mother'],
  accountType: ['Facebook', 'Twitter', 'LinkedIn'],
  languageName: ['French', 'English'],
  languageCode: ['French', 'English'],
  reviewRating: ['Good', 'Bad'],
};

module.exports = {
  GeneratePeopleListing() {
    faker.seed(Math.floor(Math.random() * 64000) + 1000);
    let p = lClone(PeopleDTO);
    p.personMasterId = UUID();
    // Generate SourceMetaDataList
    p.sourceMetaDataList = this.GenerateSourceMetaDataList();
    p.objVersion = lSample(GeneratorOptions.objVersion);
    p.apiVersion = lSample(GeneratorOptions.apiVersion);
    p.createTimeStamp = faker.date.past(2, '2021-09-29');
    p.updateTimeStamp = faker.date.past(2, '2021-09-29');
    p = this.GenerateCommonBasicInfo(p);
    // Generate Phonenumbers
    p.phoneNumbers = this.GeneratePhoneNumbers();
    // Generate EmailAccounts
    p.emailAccounts = this.GenerateEmailAccounts();
    p.faxNumbers = this.GenerateFaxNumbers();
    // Generate Addresses
    p.addresses = this.GenerateAddresses();
    // Generate EmergencyContacts
    p.emergencyContacts = this.GenerateEmergencyContact();
    // Generate Media
    p.media = this.GenerateMedia();
    // Generate SocialMediaAccounts
    p.socialMediaAccounts = this.GenerateSocialMediaAccounts();
    // Generate WrittenLanguages
    p.writtenLanguages = this.GenerateWrittenLanguages();
    // Generate SpokenLanguages
    p.spokenLanguages = this.GenerateSpokenLanguages();
    // Geenerate languages
    p.languages = this.GenerateLanguages();
    // Generate LearningPlans
    p.learningPlans = this.GenerateLearningPlans();
    p.isVerified = faker.random.boolean();
    return p;
  },
  GenerateSourceMetaDataList() {
    const sourceMetaDataList = [];
    for (let i = 0; i < 2; i += 1) {
      sourceMetaDataList.push(
        lMerge(lClone(SourceMetaDataListDTO), {
          sourceSystemName: lSample(GeneratorOptions.sourceSystemName),
          sourceEntityName: lSample(GeneratorOptions.sourceEntityName),
          sourceUniqueId: UUID(),
          sourceSystemCode: lSample(GeneratorOptions.sourceSystemCode),
          dataAvailabilityStatus: lSample(GeneratorOptions.dataAvailabilityStatus),
        }),
      );
    }
    return sourceMetaDataList;
  },
  GeneratePhoneNumbers() {
    const phoneNumbers = [];
    phoneNumbers.push(
      lMerge(lClone(PhonenumbersDTO), {
        phoneType: lSample(GeneratorOptions.phoneType),
        phoneNumber: faker.phone.phoneNumber(),
        seqNum: faker.random.number(3),
        isPrimary: faker.random.boolean(),
        dataAvailabilityStatus: lSample(GeneratorOptions.dataAvailabilityStatus),
        dataSources: [
          {
            sourceSystemName: lSample(GeneratorOptions.sourceSystemName),
            sourceAvailabilityStatus: lSample(GeneratorOptions.dataAvailabilityStatus),
          },
        ],
      }),
    );
    return phoneNumbers;
  },
  GenerateEmailAccounts() {
    const emailAccounts = [];
    emailAccounts.push(
      lMerge(lClone(EmailAccountsDTO), {
        emailType: lSample(GeneratorOptions.emailType),
        emailAddress: faker.internet.email(),
        emailDisplayName: faker.internet.userName(),
        seqNum: faker.random.number(3),
        isPrimary: faker.random.boolean(),
        dataAvailabilityStatus: lSample(GeneratorOptions.dataAvailabilityStatus),
        dataSources: [
          {
            sourceSystemName: lSample(GeneratorOptions.sourceSystemName),
            sourceAvailabilityStatus: lSample(GeneratorOptions.dataAvailabilityStatus),
          },
        ],
      }),
    );
    return emailAccounts;
  },
  GeneratePeopleSearch() {
    faker.seed(Math.floor(Math.random() * 64000) + 1000);
    const s = lClone(SearchDTO);
    // Gennerate for people
    const people = this.GeneratePeopleListing();
    s.people.push(people);
    // Generate for staff
    const staff = this.GenerateStaff();
    s.staff.push(staff);
    return s;
  },
  GenerateStaff() {
    const s = lClone(StaffDTO);
    s.staffMasterId = UUID();
    // Generate SourceMetaDataList
    s.sourceMetaDataList = this.GenerateSourceMetaDataList();
    s.objVersion = lSample(GeneratorOptions.objVersion);
    s.apiVersion = lSample(GeneratorOptions.apiVersion);
    s.isShowOnInternet = faker.random.boolean();
    s.createTimeStamp = faker.date.past(2, '2021-09-29');
    s.updateTimeStamp = faker.date.past(2, '2021-09-29');
    // Generate BasicInfo
    s.basicPersonInfo = this.GenerateBasicInfo();
    // Generate webDisplayOptions
    s.webDisplayOptions = this.GenerateWebDisplayOptions();
    // Generate CompanyOfficeAssociations
    s.companyOfficeAssociation = this.GeneratecompanyOfficeAssociation();
    return s;
  },
  GenerateBasicInfo() {
    let b = lClone(BasicInfoDTO);
    b.personMasterId = UUID();
    b = this.GenerateCommonBasicInfo(b);
    // Generate Phonenumbers
    b.phoneNumbers = this.GeneratePhoneNumbers();
    // Generate EmailAccounts
    b.emailAccounts = this.GenerateEmailAccounts();
    // Generate FaxNumbers
    b.faxNumbers = this.GenerateFaxNumbers();
    // Generate Addresses
    b.addresses = this.GenerateAddresses();
    // Generate EmergencyContacts
    b.emergencyContacts = this.GenerateEmergencyContact();
    // Generate Media
    b.media = this.GenerateMedia();
    // Generate SocialMediaAccounts
    b.socialMediaAccounts = this.GenerateSocialMediaAccounts();
    // Generate WrittenLanguages
    b.writtenLanguages = this.GenerateWrittenLanguages();
    // Generate SpokenLanguages
    b.spokenLanguages = this.GenerateSpokenLanguages();
    // Geenerate languages
    b.languages = this.GenerateLanguages();
    // Generate LearningPlans
    b.learningPlans = this.GenerateLearningPlans();
    return b;
  },
  GenerateLearningPlans() {
    const learningPlans = [];
    learningPlans.push(
      lMerge(lClone(LearningPlansDTO), {
        actionType: faker.random.word(3),
        learningPlanId: UUID(),
      }),
    );
    return learningPlans;
  },
  GenerateLanguages() {
    const languages = [];
    languages.push(
      lMerge(lClone(LanguagesDTO), {
        code: faker.random.alphaNumeric(3),
        longDescription: faker.name.jobDescriptor(),
      }),
    );
    return languages;
  },
  GenerateWrittenLanguages() {
    const writtenLanguages = [];
    writtenLanguages.push(
      lMerge(lClone(WrittenLanguagesDTO), {
        languageCode: lSample(GeneratorOptions.languageCode),
        languageName: lSample(GeneratorOptions.languageName),
      }),
    );
    return writtenLanguages;
  },
  GenerateSpokenLanguages() {
    const spokenLanguages = [];
    spokenLanguages.push(
      lMerge(lClone(SpokenLanguagesDTO), {
        languageCode: lSample(GeneratorOptions.languageCode),
        languageName: lSample(GeneratorOptions.languageName),
      }),
    );
    return spokenLanguages;
  },
  GenerateSocialMediaAccounts() {
    const socialMedia = [];
    socialMedia.push(
      lMerge(lClone(SocialMediaAccountsDTO), {
        url: UUID(),
        accountType: lSample(GeneratorOptions.accountType),
        seqNum: faker.random.number(3),
        accountName: faker.name.findName(),
        isPrimary: faker.datatype.boolean(),
      }),
    );
    return socialMedia;
  },
  GenerateEmergencyContact() {
    const emergency = [];
    emergency.push(
      lMerge(lClone(EmergencyDTO), {
        personMasterId: UUID(),
        name: faker.name.findName(),
        phone: faker.phone.phoneNumber(),
        isPrimary: faker.datatype.boolean(),
        relation: lSample(GeneratorOptions.relation),
      }),
    );
    return emergency;
  },
  GenerateAddresses() {
    const b = lClone(AddressesDTO);
    const agent = faker.helpers.createCard();
    b.addressMasterId = UUID();
    b.addressType = lSample(GeneratorOptions.addressType);
    b.address = agent.address.street;
    b.addressLine1 = agent.address.streetA;
    b.addressLine2 = agent.address.streetB;
    b.city = agent.address.city;
    b.state = agent.address.state;
    b.zipcode = agent.address.zipcode;
    b.country = agent.address.county;
    b.valid = faker.datatype.boolean();
    b.streetLatitude = faker.address.latitude();
    b.streetLongitude = faker.address.longitude();
    b.parcelLatitude = faker.address.latitude();
    b.parcelLongitude = faker.address.longitude();
    b.addressId = UUID();
    b.sdUnifId = UUID();
    b.originalAddress = this.GenerateOriginalAddress();
    return b;
  },
  GenerateOriginalAddress() {
    const agent = faker.helpers.createCard();
    return lMerge(lClone(OriginalAddressDTO), {
      addressType: lSample(GeneratorOptions.addressType),
      addressLine1: agent.address.streetA,
      addressLine2: agent.address.streetB,
      city: agent.address.city,
      state: agent.address.state,
      zipcode: agent.address.zipcode,
      country: agent.address.county,
    });
  },
  GenerateFaxNumbers() {
    const faxNumbers = [];
    faxNumbers.push(
      lMerge(lClone(FaxNumbersDTO), {
        phoneType: lSample(GeneratorOptions.phoneType),
        phoneNumber: faker.phone.phoneNumber(),
        seqNum: faker.random.number(3),
        isPrimary: faker.random.boolean(),
      }),
    );
    return faxNumbers;
  },
  GenerateCommonBasicInfo(newVal) {
    const p = newVal;
    p.namePrefix = faker.name.prefix();
    p.firstName = faker.name.firstName();
    p.middleName = faker.name.middleName();
    p.lastName = faker.name.lastName();
    p.nameSuffix = faker.name.suffix();
    p.displayName = faker.name.findName();
    p.familiarName = faker.name.findName();
    p.nickName = faker.name.findName();
    p.preferredFirstName = p.firstName;
    p.preferredLastName = p.lastName;
    p.legalFirstName = p.firstName;
    p.legalMiddleName = p.middleName;
    p.legalLastName = p.lastName;
    p.disableVeteran = faker.random.boolean();
    p.oktaId = UUID();
    p.oktaIds = [p.oktaId];
    p.maritalStatus = lSample(GeneratorOptions.maritalStatus);
    p.gender = faker.name.gender();
    return p;
  },
  GenerateWebDisplayOptions() {
    const webOptions = [];

    webOptions.push(
      lMerge(lClone(WebOptionsDTO), {
        phoneType: lSample(GeneratorOptions.phoneType),
        displayOnWeb: faker.random.boolean(),
        dataAvailabilityStatus: lSample(GeneratorOptions.dataAvailabilityStatus),
      }),
    );
    return webOptions;
  },
  GeneratecompanyOfficeAssociation() {
    const c = lClone(CompanyOfficeAssociationsDTO);
    c.staffOktaId = UUID();
    c.staffOktaIds = [c.staffOktaId];
    c.companyMasterId = UUID();
    c.companyName = faker.company.companyName();
    c.recruitedBy = faker.name.findName();
    c.brandCode = lSample(GeneratorOptions.brandCode);
    c.brandName = lSample(GeneratorOptions.brandName);
    c.isNrt = faker.random.boolean();
    // Generate offices
    c.offices = this.GenerateOffice();
    // Generate EmailAccounts
    c.emailAccounts = this.GenerateEmailAccounts();
    // Generate phoneNumbers
    c.phoneNumbers = this.GeneratePhoneNumbers();
    // Generate Media
    c.media = this.GenerateMedia();
    c.isZipTeamflag = faker.random.boolean();
    c.isCorporateStaff = faker.random.boolean();
    return c;
  },
  GenerateOffice() {
    const o = lClone(OfficeDTO);
    o.officeMasterId = UUID();
    o.officeName = faker.company.companyName();
    o.jobTitles = this.GeneratejobTitles();
    o.isPrimaryOffice = faker.random.boolean();
    // Generate EmailAccounts
    o.emailAccounts = this.GenerateEmailAccounts();
    // Generate phoneNumbers
    o.phoneNumbers = this.GeneratePhoneNumbers();
    // Generate FaxNumbers
    o.faxNumbers = this.GenerateFaxNumbers();
    // Generate Address
    o.addresses = this.GenerateAddresses();
    // Generate SocialMediaAccounts
    o.socialMediaAccounts = this.GenerateSocialMediaAccounts();
    // Generate Media
    o.media = this.GenerateMedia();
    return o;
  },
  GeneratejobTitles() {
    const jobTitles = [];
    jobTitles.push(
      lMerge(lClone(JobTitlesDTO), {
        titleCode: faker.name.jobTitle(),
        titleDescription: faker.name.jobDescriptor(),
        isFullTime: faker.random.boolean(),
        isShowOnInternet: faker.random.boolean(),
      }),
    );
    return jobTitles;
  },
  GenerateMedia() {
    const media = [];
    media.push(
      lMerge(lClone(MediaDTO), {
        mediaType: lSample(GeneratorOptions.mediaType),
        mediaUsageType: lSample(GeneratorOptions.mediaUsageType),
        url: faker.image.imageUrl(),
        sourceMediaURL: faker.image.imageUrl(),
        seqNum: faker.random.number(3),
        dataAvailabilityStatus: lSample(GeneratorOptions.dataAvailabilityStatus),
      }),
    );
    return media;
  },
  GeneratePeopleSearchByOkta() {
    faker.seed(Math.floor(Math.random() * 64000) + 1000);
    const p = lClone(PeopleByOktaDTO);
    p.personMasterId = UUID();
    // Generate MasterEntities
    p.masterEntities = this.GenerateMasterEntities();
    return p;
  },
  GenerateMasterEntities() {
    const m = lClone(MasterEntitiesDTO);
    // Generate Agnet

    const agent = this.GenerateAgent();
    m.agent.push(agent);
    const staff = this.GenerateStaff();
    m.staff.push(staff);
    return m;
  },
  GenerateAgent() {
    const a = lClone(AgentDTO);
    a.agentMasterId = UUID();
    // Generate sourceMetaDataList
    const sourceMetaDataList = this.GenerateSourceMetaDataList();
    a.sourceMetaDataList = sourceMetaDataList.map(
      ({ sourceSystemName, sourceEntityName, sourceUniqueId }) => ({
        sourceSystemName,
        sourceEntityName,
        sourceUniqueId,
      }),
    );
    a.nrdsId = UUID();
    a.objVersion = lSample(GeneratorOptions.objVersion);
    a.apiVersion = lSample(GeneratorOptions.apiVersion);
    a.replacedByMasterId = UUID();
    a.deprecatedMasterIds = [UUID()];
    a.createTimeStamp = faker.date.past(2, '2021-09-29');
    a.updateTimeStamp = faker.date.past(2, '2021-09-29');
    // Generate BasicPersonInfo
    a.basicPersonInfo = this.GenerateBasicInfo();
    a.profile = faker.name.findName();
    a.htmlProfileText = faker.name.title();
    a.bioTitle = faker.name.findName();
    a.agentHireDate = faker.date.past(2, '2021-09-29');
    a.webDisplayOptions = this.GenerateWebDisplayOptions();
    a.startedRealEstateOn = faker.date.past(2, '2021-09-29');
    // Generate insurances
    a.insurances = this.GeneraterInsurances();
    // Generate MlsMembership
    a.mlsMemberships = this.GenerateMlsMemberships();
    // Generate companyOfficeAssociation
    a.companyOfficeAssociation = this.GeneratecompanyOfficeAssociation();
    // Generate OtherMemberships
    a.otherMemberships = this.GenerateOtherMemberships();
    a.coveredGeographicLocations = [faker.address.streetAddress()];
    a.coveredGeoPoliticalAreas = [faker.address.streetAddress()];
    a.coveredZipcodes = [faker.address.zipCode()];
    a.managementAreas = [faker.address.streetAddress()];
    a.reviewRatings = [lSample(GeneratorOptions.reviewRating)];

    // Generate Custom Attribute
    a.customAttributes = this.GenerateCustomAttributes();
    // Generate agentRiskProfiles
    a.agentRiskProfiles = this.GenerateagentRiskProfiles();
    // Generate AgentPerformances
    a.agentPerformances = this.GenerateAgentPerformances();
    // Generate Awards
    a.awards = this.GenerateAwards();
    // Generate Designations
    a.designations = this.GenerateDesignations();
    // Generate Licenses
    a.jobTitles = this.GeneratejobTitles();
    return a;
  },

  GenerateDesignations() {
    const designation = [];
    designation.push(
      lMerge(lClone(DesignationDTO), {
        designationCode: faker.random.alphaNumeric(3),
      }),
    );
    return designation;
  },
  GenerateAwards() {
    const awards = [];
    awards.push(
      lMerge(lClone(AwardsDTO), {
        awardYear: faker.datatype.number(2018, 2021),
        awardCode: faker.random.alphaNumeric(3),
      }),
    );
    return awards;
  },
  GenerateAgentPerformances() {
    const agnet = [];
    agnet.push(
      lMerge(lClone(AgentPerformanceDTO), {
        modelName: faker.lorem.word(),
        year: faker.datatype.number(2018, 2021),
        overallRank: faker.random.number(1),
        rankInMls: faker.datatype.boolean(),
        tenureWithBrand: faker.datatype.boolean(),
      }),
    );
    return agnet;
  },
  GenerateagentRiskProfiles() {
    const agnet = [];
    agnet.push(
      lMerge(lClone(AgentRiskProfileDTO), {
        name: faker.name.findName(),
        value: faker.random.number(1),
      }),
    );
    return agnet;
  },
  GenerateCustomAttributes() {
    const custom = [];
    custom.push(
      lMerge(lClone(CustomAttributesDTO), {
        name: faker.name.findName(),
        value: faker.random.number(1),
      }),
    );
    return custom;
  },
  GenerateOtherMemberships() {
    const otherMember = [];
    otherMember.push(
      lMerge(lClone(OtherMembershipsDTO), {
        id: UUID(),
        orgName: faker.random.word(),
        idType: faker.random.word(),
      }),
    );
    return otherMember;
  },
  GenerateMlsMemberships() {
    const mlsMember = [];
    mlsMember.push(
      lMerge(lClone(MlsMembershipsDTO), {
        mlsId: UUID(),
        board: faker.random.word(),
        agentMlsIds: [UUID()],
        isPrimary: faker.datatype.boolean(),
        startDate: faker.date.past(2, '2021-09-29'),
        endDate: faker.date.past(2, '2021-09-29'),
      }),
    );
    return mlsMember;
  },
  GeneraterInsurances() {
    const insurance = [];
    insurance.push(
      lMerge(lClone(InsurancesDTO), {
        insuranceTypeCode: faker.random.alphaNumeric(3),
        insuranceTypeDescription: faker.commerce.productDescription(),
        expiresOn: faker.date.past(2, '2021-09-29'),
      }),
    );
    return insurance;
  },
};
