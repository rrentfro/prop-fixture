const faker = require('faker');
const lClone = require('lodash/clone');
const lMerge = require('lodash/merge');
const AgentDTO = require('../../../dto/agents/agentsDTO');

module.exports = {
  GenerateAgent() {
    faker.seed(Math.floor(Math.random() * 64000) + 1000);
    const p = [];
    for (let i = 0; i < Math.floor(Math.random() * 10) + 1; i++) {
      p.push(
        lMerge(lClone(AgentDTO), {
          id: faker.random.uuid(),
          lastUpdateOn: faker.date.past(2, '2021-09-29'),
          action: faker.name.title(),
          entityDetail: { value: faker.name.title() },
        }),
      );
    }
    return {
      nextLink: faker.internet.url(),
      data: p,
    };
  },
};
