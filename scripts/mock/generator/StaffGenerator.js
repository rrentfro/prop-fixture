const UUID = require('uuid4');
const faker = require('faker');
const lClone = require('lodash/clone');
const lSample = require('lodash/sample');
const lMerge = require('lodash/merge');

const staffDTO = require('../../../dto/staff/staffDTO');
const BasicPersonInfoDTO = require('../../../dto/staff/BasicPersonInfoDTO');
const CompanyOfficeAssociationDTO = require('../../../dto/staff/CompanyOfficeAssociationDTO');

const GeneratorOptions = {
  staffMasterId: '',
  sourceMetaDataList: [
    {
      sourceEntityName: 'CompanyStaffGUID',
      sourceSystemName: 'dash',
      sourceUniqueId: 'A896CF97-3913-4C9E-A354-6AE01BE1F9EE',
      sourceSystemCode: 'RLG_DASH',
      dataAvailabilityStatus: 'A',
    },
    {
      sourceEntityName: 'Staff',
      sourceSystemName: 'MDM',
      sourceUniqueId: '15575505',
      dataAvailabilityStatus: 'A',
    },
  ],
  webDisplayOptions: [
    {
      phoneType: 'CellPhone',
      displayOnWeb: true,
      dataAvailabilityStatus: 'A',
    },
    {
      phoneType: 'BusinessPhone',
      displayOnWeb: true,
      dataAvailabilityStatus: 'A',
    },
  ],
  objVersion: [6],
  apiVersion: ['1.0.0'],
  createTimeStamp: ['2020-07-15T23:37:09.721'],
  updateTimeStamp: ['2021-10-01T18:30:52.781'],
  basicPersonInfo: {
    firstName: ['Teresa'],
    lastName: ['Liepman'],
    displayName: ['Teresa Liepman'],
    familiarName: ['Teresa'],
    preferredFirstName: ['Teresa'],

  },

  offices: [
    {
      officeMasterId: '',
      officeName: 'Infinity',
      isPrimaryOffice: true,
    },
  ],
  companyMasterId: '',
  companyName: ['Infinity'],
  brandCode: ['BHG'],
  brandName: ['BHG'],
  awards: [
    {
      awardCode: ['PRC'],
      awardDescription: ['Premier Company'],
    },
  ],
  designations: [
    {
      designationCode: 'ABR',
      designationDescription: 'Accredited Buyers Representative',
    },
    {
      designationCode: 'CRS',
      designationDescription: 'Certified Residential Specialist',
    },
  ],
  jobTitles: [
    {
      titleCode: 'OW',
      titleDescription: 'Owner',
      isFullTime: true,
      isShowOnInternet: true,
    },
    {
      titleCode: 'RB',
      titleDescription: 'Responsible Broker',
      isFullTime: true,
      isShowOnInternet: true,
    },
  ],

  emailAccounts: [
    {
      emailType: 'VanityEmail',
      emailAddress: 'teresa.liepman@bhgrealestate.com',
      seqNum: 1,
      dataAvailabilityStatus: 'A',
      dataSources: [
        {
          sourceSystemName: 'RLG_DASH',
          sourceAvailabilityStatus: 'A',
        },
      ],
    },
    {
      emailType: 'LeadRouterEmail',
      emailAddress: '400257.lead.413783223@leads.leadrouter.com',
      seqNum: 1,
      dataAvailabilityStatus: 'A',
      dataSources: [
        {
          sourceSystemName: 'RLG_DASH',
          sourceAvailabilityStatus: 'A',
        },
      ],
    },
    {
      emailType: 'BusinessEmail',
      emailAddress: 'teresasoldit2@gmail.com',
      seqNum: 1,
      isPrimary: true,
      dataAvailabilityStatus: 'A',
      dataSources: [
        {
          sourceSystemName: 'RLG_DASH',
          sourceAvailabilityStatus: 'A',
        },
      ],
    },
  ],
  phoneNumbers: [
    {
      phoneType: 'CellPhone',
      phoneNumber: '+19032769464',
      seqNum: 1,
      dataAvailabilityStatus: 'A',
      dataSources: [
        {
          sourceSystemName: 'RLG_DASH',
          sourceAvailabilityStatus: 'A',
        },
      ],
    },
    {
      phoneType: 'BusinessPhone',
      phoneNumber: '+19033061454',
      seqNum: 1,
      isPrimary: true,
      dataAvailabilityStatus: 'A',
      dataSources: [
        {
          sourceSystemName: 'RLG_DASH',
          sourceAvailabilityStatus: 'A',
        },
      ],
    },
  ],
  media: [
    {
      mediaType: 'Image',
      mediaUsageType: 'PersonPhoto',
      url: 'http://m.bhgrealestate.com/1103i0/gfdgxte6fz5q46646y43gv64z0i',
      cdnURL: 'https://images.uat.eapdeprd.realogyprod.com/uat-staff/BHG/Q00700000FDEvTR0mi9GUPfk44k6GZ2ECDLR9arR/Q01600000FYKNjjHb0ZSGS82XIk778FOOa5CRDqK.jpg',
      sourceMediaURL: 'http://imgs.azureedge.net/5A6E4C8E-9D14-4A91-8CE5-E6EDEDA53F07',
      seqNum: 1,
      dataAvailabilityStatus: 'A',
    },
  ],
};

module.exports = {
  GenerateStaff(options = {}) {
    faker.seed(Math.floor(Math.random() * 64000) + 1000);
    const Staff = lClone(staffDTO);
    Staff.staffMasterId = UUID();

    const sourceMetaDataList = [];
    for (let i = 0; i < Math.floor(Math.random() * 3) + 1; i += 1) {
      sourceMetaDataList.push(lSample(GeneratorOptions.sourceMetaDataList));
      sourceMetaDataList[i].sourceUniqueId = UUID();
    }

    Staff.sourceMetaDataList = sourceMetaDataList;

    const webDisplayOptions = [];
    for (let i = 0; i < Math.floor(Math.random() * 3) + 1; i += 1) {
      webDisplayOptions.push(lSample(GeneratorOptions.webDisplayOptions));
    }
    Staff.webDisplayOptions = webDisplayOptions;

    if (!Object.prototype.hasOwnProperty.call(options, 'basicPersonInfo') || options.basicPersonInfo !== false) {
      Staff.basicPersonInfo = lMerge(lClone(BasicPersonInfoDTO), {
        personMasterId: UUID(),
        firstName: lSample(GeneratorOptions.basicPersonInfo.firstName),
        lastName: lSample(GeneratorOptions.basicPersonInfo.lastName),
        displayName: lSample(GeneratorOptions.basicPersonInfo.displayName),
        familiarName: lSample(GeneratorOptions.basicPersonInfo.familiarName),
        preferredFirstName: lSample(GeneratorOptions.basicPersonInfo.preferredFirstName),
      });

      const emailAccounts = [];
      for (let i = 0; i < Math.floor(Math.random() * 3) + 1; i += 1) {
        emailAccounts.push(lSample(GeneratorOptions.emailAccounts));
      }
      Staff.basicPersonInfo.emailAccounts = emailAccounts;

      const phoneNumbers = [];
      for (let i = 0; i < Math.floor(Math.random() * 3) + 1; i += 1) {
        phoneNumbers.push(lSample(GeneratorOptions.phoneNumbers));
      }
      Staff.basicPersonInfo.phoneNumbers = phoneNumbers;

      const media = [];
      for (let i = 0; i < Math.floor(Math.random() * 3) + 1; i += 1) {
        media.push(lSample(GeneratorOptions.media));
      }
      Staff.basicPersonInfo.media = media;
    } else {
      Staff.basicPersonInfo = null;
    }

    if (!Object.prototype.hasOwnProperty.call(options, 'companyOfficeAssociation') || options.companyOfficeAssociation !== false) {
      const staffOktaId = UUID();
      Staff.companyOfficeAssociation = lMerge(lClone(CompanyOfficeAssociationDTO), {
        staffOktaId,
        staffOktaIds: [staffOktaId],
        companyMasterId: UUID(),

        companyName: lSample(GeneratorOptions.companyName),
        brandCode: lSample(GeneratorOptions.brandCode),
        brandName: lSample(GeneratorOptions.brandName),
      });

      const offices = [];
      for (let i = 0; i < Math.floor(Math.random() * 3) + 1; i += 1) {
        offices.push(lSample(GeneratorOptions.offices));
        offices[i].officeMasterId = UUID();
      }
      Staff.companyOfficeAssociation.offices = offices;

      const emailAccounts = [];
      for (let i = 0; i < Math.floor(Math.random() * 3) + 1; i += 1) {
        emailAccounts.push(lSample(GeneratorOptions.emailAccounts));
      }
      Staff.companyOfficeAssociation.emailAccounts = emailAccounts;

      const phoneNumbers = [];
      for (let i = 0; i < Math.floor(Math.random() * 3) + 1; i += 1) {
        phoneNumbers.push(lSample(GeneratorOptions.phoneNumbers));
      }
      Staff.companyOfficeAssociation.phoneNumbers = phoneNumbers;
    } else {
      Staff.basicPersonInfo = null;
    }

    if (!Object.prototype.hasOwnProperty.call(options, 'awards') || options.awards !== false) {
      const awards = [];
      for (let i = 0; i < Math.floor(Math.random() * 3) + 1; i += 1) {
        awards.push(lSample(GeneratorOptions.awards));
      }
      Staff.awards = awards;
    } else {
      Staff.awards = null;
    }

    if (!Object.prototype.hasOwnProperty.call(options, 'designations') || options.designations !== false) {
      const designations = [];
      for (let i = 0; i < Math.floor(Math.random() * 3) + 1; i += 1) {
        designations.push(lSample(GeneratorOptions.designations));
      }
      Staff.designations = designations;
    } else {
      Staff.designations = null;
    }

    if (!Object.prototype.hasOwnProperty.call(options, 'jobTitles') || options.jobTitles !== false) {
      const jobTitles = [];
      for (let i = 0; i < Math.floor(Math.random() * 3) + 1; i += 1) {
        jobTitles.push(lSample(GeneratorOptions.jobTitles));
      }
      Staff.jobTitles = jobTitles;
    } else {
      Staff.jobTitles = null;
    }

    return Staff;
  },

};
