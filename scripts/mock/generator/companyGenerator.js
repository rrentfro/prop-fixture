const UUID = require('uuid4');
const faker = require('faker');
const lClone = require('lodash/clone');
const lSample = require('lodash/sample');
const lMerge = require('lodash/merge');
const CompanyDTO = require('../../../dto/company/CompanyDTO');
const OfficeDTO = require('../../../dto/company/OfficeDTO');
const SourceMetaDTO = require('../../../dto/company/SourceMetaDTO');

const GeneratorOptions = {

  sourceSystemName: ['dash', 'MDM', 'CBHomes'],
  sourceEntityName: ['CompanyGUID', 'CompanyCode', 'CompanyID', 'ProviderIdentifier', 'Company'],
  sourceSystemCode: ['RLG_DASH', 'RLG_CBH'],
  sourceUniqueId: [902, 68105599, 655],
  dataAvailabilityStatus: ['A'],

  companyName: ['Infinity', 'Hammond', 'Laura McCarthy'],
  brandName: ['BHG', 'HAM', 'LMC'],
  brandCode: ['BAG', 'HAM', 'LMC'],
  brandMemberSince: ['2018-11-20T00:00:00', '2017-11-20T00:00:00'],
  anniversaryDate: ['2018-11-20T00:00:00', '2017-11-20T00:00:00'],
  awardState: ['TX'],
  awardsRegion: ['North Texas', 'South Texas'],
  businessType: ['Residential', 'Commercial'],

  officeName: ['Infinity', 'Chestnut Hill - Hammond', 'Clayton - Laura McCarthy', 'REO'],
  officeType: ['Main office', 'Other Office'],
  officeTypeCode: ['MO', 'OO'],

  defaultLanguage: [2996],
  isEpay: [true],
  objVersion: [6],
  apiVersion: ['1.0.0'],
  createTimeStamp: ['2020-07-15T23:31:07.381'],
  updateTimeStamp: ['2021-08-10T15:37:52.999'],
};

module.exports = {
  GenerateCompnay(options = {}) {
    faker.seed(Math.floor(Math.random() * 64000) + 1000);
    const C = lClone(CompanyDTO);
    C.companyMasterId = UUID();
    if (!Object.prototype.hasOwnProperty.call(options, 'sourceMetaDataList') || options.sourceMetaDataList !== false) {
      // Generate listing
      for (let i = 0; i < faker.datatype.number(1, 5); i += 1) {
        C.sourceMetaDataList.push(this.GenerateSourceMeta(options));
      }
    } else {
      C.sourceMetaDataList = null;
    }

    if (!Object.prototype.hasOwnProperty.call(options, 'officeList') || options.officeList !== false) {
      // Generate listing
      for (let i = 0; i < faker.datatype.number(1, 5); i += 1) {
        C.officeList.push(this.GenerateOffice(options));
      }
    } else {
      C.officeList = null;
    }
    C.companyName = lSample(GeneratorOptions.companyName);
    C.brandName = lSample(GeneratorOptions.brandName);
    C.brandCode = lSample(GeneratorOptions.brandCode);
    C.brandMemberSince = lSample(GeneratorOptions.brandMemberSince);
    C.anniversaryDate = lSample(GeneratorOptions.anniversaryDate);
    C.awardState = lSample(GeneratorOptions.awardState);
    C.awardsRegion = lSample(GeneratorOptions.awardsRegion);
    C.businessType = lSample(GeneratorOptions.businessType);

    C.defaultLanguage = lSample(GeneratorOptions.defaultLanguage);
    C.isEpay = lSample(GeneratorOptions.isEpay);
    C.objVersion = lSample(GeneratorOptions.objVersion);
    C.apiVersion = lSample(GeneratorOptions.apiVersion);
    C.createTimeStamp = lSample(GeneratorOptions.createTimeStamp);
    C.updateTimeStamp = lSample(GeneratorOptions.updateTimeStamp);

    return C;
  },

  GenerateSourceMeta(options = []) {
    console.log(options);
    return lMerge(lClone(SourceMetaDTO), {
      sourceUniqueId: UUID(),
      sourceSystemName: lSample(GeneratorOptions.sourceSystemName),
      sourceEntityName: lSample(GeneratorOptions.sourceEntityName),
      sourceSystemCode: lSample(GeneratorOptions.sourceSystemCode),
      dataAvailabilityStatus: lSample(GeneratorOptions.dataAvailabilityStatus),
    });
  },

  GenerateOffice(options = []) {
    console.log(options);
    return lMerge(lClone(OfficeDTO), {
      officeMasterId: UUID(),
      officeName: lSample(GeneratorOptions.officeName),
      officeType: lSample(GeneratorOptions.officeType),
      sourceSystemCode: lSample(GeneratorOptions.sourceSystemCode),
      officeTypeCode: lSample(GeneratorOptions.officeTypeCode),
    });
  },
};
