const UUID = require('uuid4');
const faker = require('faker');
const lClone = require('lodash/clone');
const lSample = require('lodash/sample');
const lMerge = require('lodash/merge');

const CompanyDTO = require('../../dto/office/CompanyDTO.json');
const OfficeDTO = require('../../dto/office/OfficeDTO.json');
const AddressDTO = require('../../dto/office/AddressDTO.json');
const OriginalAddressDTO = require('../../dto/office/OriginalAddressDTO.json');

const GeneratorOptions = {
  officeMasterId: UUID(),
  officeName: ['Infinity', 'Chestnut Hill - Hammond', 'Clayton - Laura McCarthy', 'REO'],
  officeTypeCode: ['MO', 'OO'],
  officeType: ['Main office', 'Other Office'],
  profile: ['TBD'],
  doingBusinessAs: ['Better Homes and Gardens Real Estate Infinity'],
  sourceMetaDataList: [
    {
      sourceUniqueId: UUID(),
      sourceSystemName: 'dash',
      sourceEntityName: 'OfficeGUID',
      sourceSystemCode: 'RLG_DASH',
      dataAvailabilityStatus: 'A',
    },
    {
      sourceUniqueId: '0001',
      sourceSystemName: 'dash',
      sourceEntityName: 'OfficeID',
      sourceSystemCode: 'RLG_DASH',
      dataAvailabilityStatus: 'A',
    },
    {
      sourceUniqueId: '5_5001107',
      sourceSystemName: 'dash',
      sourceEntityName: 'ProviderIdentifier',
      sourceSystemCode: 'RLG_DASH',
      dataAvailabilityStatus: 'A',
    },
    {
      sourceUniqueId: '14981861',
      sourceSystemName: 'MDM',
      sourceEntityName: 'Office',
      dataAvailabilityStatus: 'A',
    },
  ],
  company: {
    companyMasterId: '',
    companyName: ['Infinity'],
    brandCode: ['BHG', 'HAM', 'LMC'],
    brandName: ['BHG', 'HAM', 'LMC'],
  },
  emailAccounts: [
    {
      emailType: 'AwardsMailingEmail',
      emailAddress: 'teresa@sellingtexarkana.com',
      seqNum: 1,
      dataAvailabilityStatus: 'A',
      dataSources: [
        {
          sourceSystemName: 'RLG_DASH',
          sourceAvailabilityStatus: 'A',
        },
      ],
    },
    {
      emailType: 'LeadRouterEmail',
      emailAddress: '400257.lead@leads.leadrouter.com',
      seqNum: 1,
      dataAvailabilityStatus: 'A',
      dataSources: [
        {
          sourceSystemName: 'RLG_DASH',
          sourceAvailabilityStatus: 'A',
        },
      ],
    },
    {
      emailType: 'BillingAddressEmail',
      emailAddress: 'teresa@sellingtexarkana.com',
      seqNum: 1,
      dataAvailabilityStatus: 'A',
      dataSources: [
        {
          sourceSystemName: 'RLG_DASH',
          sourceAvailabilityStatus: 'A',
        },
      ],
    },
    {
      emailType: 'BusinessEmail',
      emailAddress: 'teresa@sellingtexarkana.com',
      seqNum: 1,
      isPrimary: true,
      dataAvailabilityStatus: 'A',
      dataSources: [
        {
          sourceSystemName: 'RLG_DASH',
          sourceAvailabilityStatus: 'A',
        },
      ],
    },
  ],
  phoneNumbers: [
    {
      phoneType: 'BusinessPhone2',
      phoneNumber: '+19032769464',
      seqNum: 1,
      dataAvailabilityStatus: 'A',
      dataSources: [
        {
          sourceSystemName: 'RLG_DASH',
          sourceAvailabilityStatus: 'A',
        },
      ],
    },
    {
      phoneType: 'BusinessPhone',
      phoneNumber: '+19033061454',
      seqNum: 1,
      isPrimary: true,
      dataAvailabilityStatus: 'A',
      dataSources: [
        {
          sourceSystemName: 'RLG_DASH',
          sourceAvailabilityStatus: 'A',
        },
      ],
    },
  ],
  areasServed: [
    {
      zipcode: '71730',
    },
    {
      zipcode: '71753',
    },
    {
      zipcode: '71801',
    },
    {
      zipcode: '71822',
    },
  ],
  media: [
    {
      mediaType: 'Image',
      mediaUsageType: 'ListTracLogo',
      url: 'http://m.bhgrealestate.com/5i0/1jqd87ghyjnnm46f1bk7cbvzq7i',
      cdnURL:
        'https://images.uat.eapdeprd.realogyprod.com/uat-offices/BHG/Q00400000FDEncskbuP4hND9sPk79cboaiNJaA3w/logos/Q01600000FPTUqfBqJeiGOBfeDYHNyr6oSpONZQf.jpg',
      sourceMediaURL: 'http://imgs.azureedge.net/356D9AD3-6268-435C-983D-73DC645DA753',
      seqNum: 1,
      dataAvailabilityStatus: 'A',
    },
    {
      mediaType: 'Image',
      mediaUsageType: 'OfficeLogoVertical',
      url: 'http://m.bhgrealestate.com/1103i0/fph5axv99ezy4wpfz6a5eees52i',
      cdnURL:
        'https://images.uat.eapdeprd.realogyprod.com/uat-offices/BHG/Q00400000FDEncskbuP4hND9sPk79cboaiNJaA3w/logos/Q01600000FPTUqo2Jv3I34mup1McntLZaK5tgp1q.jpg',
      sourceMediaURL: 'http://imgs.azureedge.net/A8D52244-813F-49A5-8FD7-B26F9E217FCC',
      seqNum: 1,
      dataAvailabilityStatus: 'A',
    },
    {
      mediaType: 'Image',
      mediaUsageType: 'OfficeLogoHorizontal',
      url: 'http://m.bhgrealestate.com/1103i0/f0e1gxjz8ccamnpyqe7zxn5842i',
      cdnURL:
        'https://images.uat.eapdeprd.realogyprod.com/uat-offices/BHG/Q00400000FDEncskbuP4hND9sPk79cboaiNJaA3w/logos/Q01600000FPTUpo6yhHFYGaCbOgCsVuvMyCfS6WJ.jpg',
      sourceMediaURL: 'http://imgs.azureedge.net/5576C86A-045B-4432-9F80-58F74FEE1EB3',
      seqNum: 1,
      dataAvailabilityStatus: 'A',
    },
    {
      mediaType: 'Image',
      mediaUsageType: 'OfficeDBALogoHorizontalGreenTransparent',
      url: 'http://m.bhgrealestate.com/1103i0/y9wrk2tz4wjjmt24cfwxthr6g7i',
      cdnURL:
        'https://images.uat.eapdeprd.realogyprod.com/uat-offices/BHG/Q00400000FDEncskbuP4hND9sPk79cboaiNJaA3w/logos/Q01600000FPTUqWLMiG8ThaQTPjvy4MlNL6Hjf8x.jpg',
      sourceMediaURL: 'http://imgs.azureedge.net/AE4369FC-5065-4119-BF87-B747BE4E172C',
      seqNum: 1,
      dataAvailabilityStatus: 'A',
    },
  ],
  defaultLanguage: ['English'],
  address: {
    addressMasterId: UUID(),
    addressType: ['Billing'],
    address: ['4602 McKnight Rd'],
    addressLine1: ['4602 McKnight Road'],
    city: ['Texarkana'],
    state: ['TX'],
    zipCode: ['75503'],
    country: ['US'],
    valid: true,
    seqNum: 1,
    originalAddress: {
      addressType: ['Billing'],
      addressLine1: ['4602 McKnight Road'],
      city: ['Texarkana'],
      state: ['TX'],
      zipcode: ['75503'],
      country: ['US'],
      dataAvailabilityStatus: ['A'],
    },
  },
  addresses: [
    {
      addressMasterId: 'Q01000000FArfygXh3kAXpqc3ZRojINvOjutdbTV',
      addressType: 'Physical',
      address: '4602 McKnight Rd',
      addressLine1: '4602 McKnight Road',
      city: 'Texarkana',
      state: 'TX',
      zipCode: '75503',
      country: 'US',
      valid: true,
      seqNum: 1,
      originalAddress: {
        addressType: 'Physical',
        addressLine1: '4602 McKnight Road',
        city: 'Texarkana',
        state: 'TX',
        zipcode: '75503',
        country: 'US',
        dataAvailabilityStatus: 'A',
      },
    },
    {
      addressMasterId: 'Q01000000FArfygXh3kAXpqc3ZRojINvOjutdbTV',
      addressType: 'Billing',
      address: '4602 McKnight Rd',
      addressLine1: '4602 McKnight Road',
      city: 'Texarkana',
      state: 'TX',
      zipCode: '75503',
      country: 'US',
      valid: true,
      seqNum: 1,
      originalAddress: {
        addressType: 'Billing',
        addressLine1: '4602 McKnight Road',
        city: 'Texarkana',
        state: 'TX',
        zipcode: '75503',
        country: 'US',
        dataAvailabilityStatus: 'A',
      },
    },
  ],
  saleOffice: true,
  isSalesOffice: true,
  objVersion: 6,
  apiVersion: ['1.0.0'],
  createTimeStamp: ['2020-07-15T23:33:11.214'],
  updateTimeStamp: ['2021-10-01T17:44:03.027'],
  totalAgents: 20,
  totalStaff: 14,
};

module.exports = {
  GenerateOffice(options = {}) {
    faker.seed(Math.floor(Math.random() * 64000) + 1000);
    const O = lClone(OfficeDTO);
    O.officeMasterId = UUID();

    O.officeName = lSample(GeneratorOptions.officeName);
    O.officeTypeCode = lSample(GeneratorOptions.officeTypeCode);
    O.officeType = lSample(GeneratorOptions.officeType);
    O.profile = lSample(GeneratorOptions.profile);
    O.doingBusinessAs = lSample(GeneratorOptions.doingBusinessAs);
    for (let i = 0; i < Math.floor(Math.random() * 3) + 1; i += 1) {
      O.sourceMetaDataList.push(lSample(GeneratorOptions.sourceMetaDataList));
    }

    if (!Object.prototype.hasOwnProperty.call(options, 'company') || options.company !== false) {
      O.company = lMerge(lClone(CompanyDTO), {
        companyMasterId: UUID(),
        companyName: lSample(GeneratorOptions.company.companyName),
        brandCode: lSample(GeneratorOptions.company.brandCode),
        brandName: lSample(GeneratorOptions.company.brandName),
      });
    } else {
      O.company = null;
    }

    for (let i = 0; i < Math.floor(Math.random() * 3) + 1; i += 1) {
      O.emailAccounts.push(lSample(GeneratorOptions.emailAccounts));
    }
    for (let i = 0; i < Math.floor(Math.random() * 3) + 1; i += 1) {
      O.phoneNumbers.push(lSample(GeneratorOptions.phoneNumbers));
    }
    for (let i = 0; i < Math.floor(Math.random() * 3) + 1; i += 1) {
      O.areasServed.push(lSample(GeneratorOptions.areasServed));
    }
    for (let i = 0; i < Math.floor(Math.random() * 3) + 1; i += 1) {
      O.media.push(lSample(GeneratorOptions.media));
    }

    O.defaultLanguage = lSample(GeneratorOptions.defaultLanguage);

    if (!Object.prototype.hasOwnProperty.call(options, 'address') || options.address !== false) {
      O.address = lMerge(lClone(AddressDTO), {
        addressMasterId: UUID(),
        addressType: lSample(GeneratorOptions.address.addressType),
        address: lSample(GeneratorOptions.address.address),
        addressLine1: lSample(GeneratorOptions.address.addressLine1),
        city: lSample(GeneratorOptions.address.city),
        zipCode: lSample(GeneratorOptions.address.zipCode),
        country: lSample(GeneratorOptions.address.country),
        valid: lSample(GeneratorOptions.address.valid),
        seqNum: lSample(GeneratorOptions.address.seqNum),
      });
    } else {
      O.address = null;
    }

    if (
      !Object.prototype.hasOwnProperty.call(options, 'originalAddress') ||
      options.originalAddress !== false
    ) {
      O.address.originalAddress = lMerge(lClone(OriginalAddressDTO), {
        addressType: lSample(GeneratorOptions.address.originalAddress.addressType),
        addressLine1: lSample(GeneratorOptions.address.originalAddress.addressLine1),
        city: lSample(GeneratorOptions.address.originalAddress.city),
        state: lSample(GeneratorOptions.address.originalAddress.state),
        zipCode: lSample(GeneratorOptions.address.originalAddress.zipCode),
        country: lSample(GeneratorOptions.address.originalAddress.country),
        dataAvailabilityStatus: lSample(
          GeneratorOptions.address.originalAddress.dataAvailabilityStatus,
        ),
      });
    } else {
      O.address.originalAddress = null;
    }

    for (let i = 0; i < Math.floor(Math.random() * 3) + 1; i += 1) {
      O.addresses.push(lSample(GeneratorOptions.addresses));
    }
    return O;
  },
};
