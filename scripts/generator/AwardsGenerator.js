const faker = require('faker');
const lClone = require('lodash/clone');
const lSample = require('lodash/sample');
const lMerge = require('lodash/merge');
const AwardsDTO = require('../../dto/awards/awardsDTO.json');

const award = [
  '2020 CENTURION® Producer',
  '2020 CENTURION® Team',
  '2020 GOLD MEDALLION® Office',
  '2020 Masters Diamond Producer',
  '2020 Masters Emerald Producer',
  '2020 Masters Ruby Producer',
  '2020 Masters Team®',
  '2020 Per Person Productivity Award Company',
];
module.exports = {
  GenerateGrantsAwards() {
    faker.seed(Math.floor(Math.random() * 64000) + 1000);
    const ProgressAwards = [];
    for (let i = 0; i < Math.floor(Math.random() * 10) + 1; i++) {
      ProgressAwards.push(
        lMerge(lClone(AwardsDTO.progressAward), {
          DIMAwardProgressKey: faker.datatype.uuid(),
          lEntity_fk: faker.random.number(),
          lPeriodicDate_fk: faker.random.word(),
          AwardOrder: faker.random.word(),
          Award: lSample(award),
          DesignationNextUnits: `${Math.floor(Math.random() * (100 - 1 + 1) + 1)}.${Math.floor(
            Math.random() * (100 - 1 + 1) + 1,
          )}`,
          GrossCommProgress: `${Math.floor(Math.random() * (100 - 1 + 1) + 1)}.${Math.floor(
            Math.random() * (1000 - 1 + 1) + 1,
          )}`,
          UnitsProgress: `${Math.floor(Math.random() * (100 - 1 + 1) + 1)}.${Math.floor(
            Math.random() * (1000 - 1 + 1) + 1,
          )}`,
          VolumeProgress: `${Math.floor(Math.random() * (100 - 1 + 1) + 1)}.${Math.floor(
            Math.random() * (1000 - 1 + 1) + 1,
          )}`,
          Progress: `${Math.floor(Math.random() * (100 - 1 + 1) + 1)}.${Math.floor(
            Math.random() * (1000 - 1 + 1) + 1,
          )}`,
          Status: 'OP',
          CreatedOn: faker.date.past(2, '2021-09-29'),
          LastUpdatedOn: faker.date.past(2, '2021-07-29'),
        }),
      );
    }
    return ProgressAwards;
  },
  GenerateProgressAwards() {
    faker.seed(Math.floor(Math.random() * 64000) + 1000);
    const ProgressAwards = [];
    for (let i = 0; i < Math.floor(Math.random() * 10) + 1; i++) {
      ProgressAwards.push(
        lMerge(lClone(AwardsDTO.progressAward), {
          DIMAwardProgressKey: faker.datatype.uuid(),
          lEntity_fk: faker.random.number(),
          lPeriodicDate_fk: faker.random.word(),
          AwardOrder: faker.random.word(),
          Award: lSample(award),
          DesignationNextUnits: `${Math.floor(Math.random() * (100 - 1 + 1) + 1)}.${Math.floor(
            Math.random() * (100 - 1 + 1) + 1,
          )}`,
          GrossCommProgress: `${Math.floor(Math.random() * (100 - 1 + 1) + 1)}.${Math.floor(
            Math.random() * (1000 - 1 + 1) + 1,
          )}`,
          UnitsProgress: `${Math.floor(Math.random() * (100 - 1 + 1) + 1)}.${Math.floor(
            Math.random() * (1000 - 1 + 1) + 1,
          )}`,
          VolumeProgress: `${Math.floor(Math.random() * (100 - 1 + 1) + 1)}.${Math.floor(
            Math.random() * (1000 - 1 + 1) + 1,
          )}`,
          Progress: `${Math.floor(Math.random() * (100 - 1 + 1) + 1)}.${Math.floor(
            Math.random() * (1000 - 1 + 1) + 1,
          )}`,
          Status: 'OP',
          CreatedOn: faker.date.past(2, '2021-09-29'),
          LastUpdatedOn: faker.date.past(2, '2021-07-29'),
        }),
      );
    }
    return ProgressAwards;
  },
};
