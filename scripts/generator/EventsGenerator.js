const faker = require('faker');
const lClone = require('lodash/clone');
const lSample = require('lodash/sample');
const lMerge = require('lodash/merge');
const eventsDTO = require('../../dto/events/eventsDTO.json');

const eventName = [
  'CENTURY 21 Main Street Realty Inc',
  'CENTURY 21 SUNBELT REALTY',
  'CENTURY 21 Frontier Realty',
  'CENTURY 21 Durden & Kornegay Realty, Inc',
  'CENTURY 21 Advantage Gold',
  'CENTURY 21 Legacy',
  'CENTURY 21 Beggins Enterprises',
  'CENTURY 21 John T. Ferreira & Son, Inc.',
  'CENTURY 21 Scheetz',
  'CENTURY 21 Town & Country',
  'CENTURY 21 Coleman-Hornsby',
  'CENTURY 21 Dairyland Realty',
  'CENTURY 21 Desert Rock',
  'CENTURY 21 Real Estate Center',
  'CENTURY 21 North Homes Realty',
  'CENTURY 21 Arizona Foothills',
];
const award = [
  '2020 CENTURION® Producer',
  '2020 CENTURION® Team',
  '2020 GOLD MEDALLION® Office',
  '2020 Masters Diamond Producer',
  '2020 Masters Emerald Producer',
  '2020 Masters Ruby Producer',
  '2020 Masters Team®',
  '2020 Per Person Productivity Award Company',
];
const agentId = ['10029', '10288', '10006', '10149', '10149', '10161', '10186'];
const personGuid = [
  '608770',
  '608770',
  '937423',
  '1075648',
  '1075648',
  '1188534',
  '1206433',
  '1206433',
  '402891471',
  '407644853',
];
const agentName = [
  'Lisa',
  'Alexander',
  'Ron',
  'Collin',
  'William',
  'Colin',
  'Maria',
  'Laurie',
  'Maureen',
  'Alice',
  'Michael',
  'Mary Jane',
  'Eileen',
  'Kimberly',
];
module.exports = {
  GenerateListOfEvents() {
    faker.seed(Math.floor(Math.random() * 64000) + 1000);
    const ListOfEvents = [];
    for (let i = 0; i < Math.floor(Math.random() * 10) + 1; i++) {
      ListOfEvents.push(
        lMerge(lClone(eventsDTO.eventListByAwards), {
          eventDate: faker.date.past(2, '2021-08-29'),
          eventName: lSample(eventName),
          eventAddress: {
            addressLine1: faker.address.streetName(),
            addressLine2: faker.address.streetAddress(),
            city: faker.address.city(),
            state: faker.address.state(),
            zip: faker.address.zipCode(),
          },
          Award: lSample(award),
          CreatedOn: faker.date.past(2, '2021-09-29'),
          LastUpdatedOn: faker.date.past(2, '2021-07-29'),
        }),
      );
    }
    return ListOfEvents;
  },
  GenerateListOfEventsByAgent() {
    faker.seed(Math.floor(Math.random() * 64000) + 1000);
    const ListOfEventsByAgent = [];
    for (let i = 0; i < Math.floor(Math.random() * 10) + 1; i++) {
      ListOfEventsByAgent.push(
        lMerge(lClone(eventsDTO.eventListdoneByAgent), {
          eventDate: faker.date.past(2, '2021-08-29'),
          eventName: lSample(eventName),
          eventAddress: {
            addressLine1: faker.address.streetName(),
            addressLine2: faker.address.streetAddress(),
            city: faker.address.city(),
            state: faker.address.state(),
            zip: faker.address.zipCode(),
          },
          Award: lSample(award),
          agentId: lSample(agentId),
          agentName: lSample(agentName),
          personGuid: lSample(personGuid),
          CreatedOn: faker.date.past(2, '2021-09-29'),
          LastUpdatedOn: faker.date.past(2, '2021-07-29'),
        }),
      );
    }
    return ListOfEventsByAgent;
  },
};
