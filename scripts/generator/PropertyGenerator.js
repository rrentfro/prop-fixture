const UUID = require('uuid4');
const faker = require('faker');
const lClone = require('lodash/clone');
const lSample = require('lodash/sample');
const lMerge = require('lodash/merge');
const crypto = require('crypto');
const PropertyDTO = require('../../dto/property/PropertyDTO.json');
const FinancialDTO = require('../../dto/property/FinancialDTO.json');
const ListingDTO = require('../../dto/property/ListingDTO.json');
const TaxDTO = require('../../dto/property/TaxDTO.json');
const HOADTO = require('../../dto/property/HOADTO.json');
const LocationDTO = require('../../dto/property/LocationDTO.json');
const StructureDTO = require('../../dto/property/StructureDTO.json');
const MediaInfoDTO = require('../../dto/property/MediaInfoDTO.json');
const CharacteristicsDTO = require('../../dto/property/CharacteristicsDTO.json');
const UtilitiesDTO = require('../../dto/property/UtilitiesDTO.json');
const EquipmentDTO = require('../../dto/property/EquipmentDTO.json');

const GeneratorOptions = {
  propertyType: ['LAND', 'SFR', 'CONDO', 'APARTMENT', 'TOWNHOUSE'],
  propertySubType: [''],
  rentIncludes: [
    'Utilities',
    'Water & Gas',
    'Water, Gas, Electric',
    'Heating',
    'Gas',
    'Pool Access',
    '',
  ],
  sourceSystemKey: ['SOLO', 'MO_MARIS'],
  standardStatus: ['ACTIVE', 'INACTIVE'],
  buildingPermits: [''],
  documentsAvailable: [''],
  disclosures: [''],
  currentFinancing: [''],
  rdmSourceSystemKey: [''],
  // Misc
  phoneType: ['OFFICE', 'MOBILE'],
  zoning: ['R3', ''],
  agentType: [''],
  mlsStatus: [''],
  taxTract: ['', 'Section 30 Township 47 R 7', 'Lakeside Hills 1', 'none'],
  associationFeeFrequency: ['', 'Annually', 'Quarterly'],
  associationAmenities: ['', 'Pool & Club', 'Park & Playground', 'DVD Rentals'],
  associationFeeIncludes: ['', 'Pool Pass', 'Park Pass'],
  petsAllowed: ['', 'Small Dogs', 'Dogs', 'Dogs & Cats', 'Chickens', 'No Pets'],
  mapCoordinate: [''],
  neighborhoodId: [0],
  streetDirPrefix: [''],
  streetDirSuffix: [''],
  mlsAreaMajor: [
    '',
    'MOON - Moonridge',
    '336 - Cathedral City South',
    '340 - Desert Hot Springs',
    '334 - Palm Springs South End',
  ],
  subdivisionName: [''],
  architectureStyle: [
    '',
    'Contemporary',
    'Mid Century, Ranch',
    'Traditional',
    'Contemporary, Mid Century',
    'Farm House',
    'Ranch',
  ],
  heating: [
    '',
    'Central',
    'Forced Air, Electric',
    'Central, Fireplace(s), Zoned, Natural Gas',
    'Forced Air, Natural Gas',
    'Air Conditioning, Ceiling Fan(s), Central Air, Zoned',
  ],
  cooling: [
    '',
    'Air Conditioning, Ceiling Fan(s)',
    'Ceiling Fan(s), Central Air',
    'Central Air',
    'Air Conditioning, Ceiling Fan(s), Central Air',
  ],
  flooring: ['', 'Carpet, Tile', 'Hardwood, Tile', 'Carpet', 'Tile', 'Carpet, Ceramic Tile'],
  constructionMaterials: ['', 'Brick', 'Stucco'],
  levels: ['', '1 Story', '2 Story', '3 Story'],
  buildingName: [''],
  buildingFeatures: [''],
  buildingAreaSource: [''],
  roof: ['', 'Composition', 'Solar', 'Clay Tile'],
  parkingFeatures: [''],
  otherParking: [
    '',
    'Driveway, 3 Covered Space(s)',
    'Covered, Direct Entrance, Garage Door Opener, Guest, On Street, Side By Side',
    'Assigned, Detached Carport, Guest, 1 Carport Space(s), 1 Covered Space(s)',
    'Other',
    'Direct Entrance, Driveway',
  ],
  otherParkingSpaces: [''],
  fireplaceFeatures: [
    '',
    '1 Fireplace, Gas Starter, Glass Doors, Tile, Great Room',
    '1 Fireplace, Stone, Living Room',
    '1 Fireplace, Gas Log, Living Room',
    '1 Fireplace, Brick, Living Room',
  ],
  doorFeatures: [''],
  foundationDetails: ['', 'Slab'],
  insulationDesc: [''],
  roomType: [
    '',
    'Bonus Room',
    'Great Room',
    'Den - Study, Great Room',
    'Billiard Room, Bonus Room',
    'Entry, Walk in Pantry',
  ],
  builderName: [''],
  accessibilityFeatures: [''],
  floodArea: [''],
  homeownersProtPlan: [''],
  exteriorFeatures: [
    '',
    'Deck',
    'Sprinkler System',
    'Rock - Stone',
    'Sprinkler Timer',
    'Awning(s), Drip System',
    'Drip System, Sprinkler System, Sprinkler Timer',
  ],
  interiorFeatures: [''],
  bodyType: ['', 'Detached, Single Family Residence', 'Attached, Condominium'],
  basement: [''],
  otherStructures: ['', 'End Unit', 'Premium Lot'],
  builderModel: [''],
  structureType: [''],
  kitchenDim: [''],
  livingRmDim: [''],
  masterBrDim: [''],
  diningRmDim: [''],
  familyRmDim: [''],
  diningDesc: [
    '',
    'Dining Area',
    'Breakfast Counter/Bar, Dining Area',
    'Breakfast Nook, Dining Area, Breakfast, Dining Room',
  ],
  familyRoomDesc: [''],
  kitchenDesc: [
    '',
    'Island, Pantry, Quartz Counters',
    'Granite Counters',
    'Island, Pantry, Stone Counters',
  ],
  livingRmDesc: [''],
  bedroomDesc: [
    '',
    'All Bedrooms Up',
    'Master Retreat, Walk In Closet',
    'Master Retreat, Walk In Closet',
    '2 Master Bedrooms, All Bedrooms Down, Main Floor Bedroom, Main Floor Master Bedroom, Master Suite, Walk In Closet',
  ],
  bathroomDesc: [
    '',
    'Double Vanity(s), Shower Over Tub',
    'Powder Room, Shower & Tub',
    'Double Shower, Remodeled, Shower & Tub, Shower Over Tub',
    'Double Vanity(s)',
  ],
  lotFeatures: [
    '',
    'Backs to Common Ground, Backs to Open Ground, Level Lot',
    'Chain Link Fence, Fencing, Level Lot, Streetlights',
    'Chain Link Fence, Fencing, Level Lot, Streetlights, Wood Fence',
    '0-1 Unit/Acre, Close to Clubhouse, Near Public Transit, Zero Lot Line',
    'Acreage, County Street, Wooded',
    'Access Type - Road, Acreage, County Street, Timberland, Wooded',
  ],
  poolFeatures: ['', 'Heated, Infinity Pool, Private', 'Heated', 'Infinity', 'Private'],
  view: ['', 'Mountains', 'Ocean'],
  laundryFeatures: [
    '',
    'Private OnPremise',
    'Dryer, Inside, Washer',
    'Dryer Included, Inside, Washer Included',
    'Dryer Included, Inside, Washer Included',
    'Gas Dryer Hookup, Inside, Upper Level, Washer Hookup',
    'Dryer Included, In Closet, Individual Room, Outside, Washer Hookup',
  ],
  spaFeatures: ['', 'Heated', 'Private', 'Spa/Hot Tub', 'Community'],
  communityFeatures: [''],
  complexName: [
    '',
    'First Services Residential',
    'Tuscany at Foothill Ranch',
    'Powerstone Property Management',
    'Tuscany',
  ],
  waterBodyName: ['', 'Lake Tahoe'],
  waterFrontFeatures: [''],
  currentUse: [''],
  possibleUse: [''],
  developmentStatus: [''],
  fencing: [''],
  roadResponsibility: [''],
  miscUtilitiesDesc: [''],
  furnished: [''],
  leaseterm: ['', '12 month', 'year', 'month to month', 'negotiable'],
  waterSource: ['', 'city', 'private', 'private well', 'Public', 'Well'],
  sewer: ['', 'Public Sewer', 'private', 'Public', 'Community Sewer, Public Sewer', 'Septic'],
  utilities: [''],
  otherEquipment: [''],
  appliances: [''],
  // @todo Review with MDP - these values are in 63033 listings - probably wrong
  securityFeatures: [
    '',
    'Cable, Composition Shingle',
    'Workshop Area',
    'Carpets, Open Floorplan',
    'Carpets, Some Wood Floors, Walk-in Closet(s)',
    'Clubhouse',
    'Carpets, High Ceilings, Open Floorplan, Walk-in Closet(s)',
  ],
  load: [''],
};

module.exports = {
  GenerateProperty(options = {}) {
    faker.seed(Math.floor(Math.random() * 64000) + 1000);
    const p = lMerge(lClone(PropertyDTO), {});

    p.property.propertyType = lSample(GeneratorOptions.propertyType);
    p.property.propertySubType = lSample(GeneratorOptions.propertySubType);
    p.property.photosCount = 0;

    if (!Object.prototype.hasOwnProperty.call(options, 'financial') || options.listing !== false) {
      // Generate financial
      const ee = Math.floor(Math.random() * 325) + 0;
      const eeSplit = Math.floor(Math.random() * 100) + 0;
      const eeSplit2 = 100 - eeSplit;

      p.property.financial = lMerge(lClone(FinancialDTO), {
        rentIncludes: lSample(GeneratorOptions.rentIncludes),
        salesIncludes: lSample(GeneratorOptions.rentIncludes),
        electricExpense: ee,
        tenantPays: ee > 0 ? Math.round((ee / 100) * eeSplit) : 0,
        ownerPays: ee > 0 ? Math.round((ee / 100) * eeSplit2) : 0,
        incomeIncludes: Math.floor(Math.random() * 200) + 0,
        isRentControl: Math.random() < 0.5,
        totalActualRent: Math.floor(Math.random() * 32000) + 125,
      });
    } else {
      p.property.financial = null;
    }

    if (!Object.prototype.hasOwnProperty.call(options, 'listing') || options.listing !== false) {
      // Generate listing
      p.property.listing = this.GenerateListing(options);
    } else {
      p.property.listing = null;
    }

    if (!Object.prototype.hasOwnProperty.call(options, 'tax') || options.tax !== false) {
      // Generate tax
      p.property.tax = lMerge(lClone(TaxDTO), {
        zoning: lSample(GeneratorOptions.zoning),
        parcelNumber: `${faker.random.alphaNumeric(3)}-${faker.random.alphaNumeric(
          2,
        )}-${faker.random.alphaNumeric(3)}`,
        taxTract: lSample(GeneratorOptions.taxTract),
        taxAnnualAmount: faker.datatype.number(1000, 100000),
        taxYear: faker.datatype.number(2018, 2021),
        taxOtherAnnualAssessmentAmount: faker.datatype.number(0, 1015),
      });
    } else {
      p.property.tax = null;
    }

    if (!Object.prototype.hasOwnProperty.call(options, 'hoa') && options.hoa !== false) {
      // Generate hoa
      p.property.hoa = lMerge(lClone(HOADTO), {
        associationFee: faker.datatype.number({ min: 0, max: 52500 }),
        associationFeeFrequency: lSample(GeneratorOptions.associationFeeFrequency),
        associationFee2: faker.datatype.number({ min: 0, max: 12500 }),
        associationFee2Frequency: lSample(GeneratorOptions.associationFeeFrequency),
        associationAmenities: lSample(GeneratorOptions.associationAmenities),
        associationFeeIncludes: lSample(GeneratorOptions.associationFeeIncludes),
        petsAllowed: lSample(GeneratorOptions.petsAllowed),
        associationName: `${faker.company.companyName()} ${faker.company.companySuffix()}`,
      });
    } else {
      p.property.hoa = null;
    }

    if (!Object.prototype.hasOwnProperty.call(options, 'location') && options.location !== false) {
      // Generate location
      p.property.location = this.GenerateLocation(options);
    } else {
      p.property.location = null;
    }

    if (
      !Object.prototype.hasOwnProperty.call(options, 'structure') &&
      options.structure !== false
    ) {
      // Generate structure
      p.property.structure = this.GenerateStructure(options);
    } else {
      p.property.structure = null;
    }

    if (
      !Object.prototype.hasOwnProperty.call(options, 'characteristics') &&
      options.characteristics !== false
    ) {
      // Generate characteristics
      p.property.characteristics = this.GenerateCharacteristics(options);
    } else {
      p.property.characteristics = null;
    }

    if (
      !Object.prototype.hasOwnProperty.call(options, 'utilities') &&
      options.utilities !== false
    ) {
      // Generate utilities
      p.property.utilities = this.GenerateUtilities(options);
    } else {
      p.property.utilities = null;
    }

    if (
      !Object.prototype.hasOwnProperty.call(options, 'equipment') &&
      options.equipment !== false
    ) {
      // Generate equipment
      p.property.equipment = this.GenerateEquipment(options);
    } else {
      p.property.equipment = null;
    }

    if (Object.prototype.hasOwnProperty.call(options, 'business') && options.business !== false) {
      // Generate business
    } else {
      p.property.business = null;
    }

    if (!Object.prototype.hasOwnProperty.call(options, 'media') && options.media !== false) {
      // Generate media
      p.media = this.GenerateMedia(options);
      p.property.photosCount = p.media.numImages;
    } else {
      p.media = null;
    }

    if (Object.prototype.hasOwnProperty.call(options, 'openHouse') && options.openHouse !== false) {
      // Generate openHouse
    } else {
      p.openHouse = null;
    }

    if (Object.prototype.hasOwnProperty.call(options, 'masterId') && options.masterId !== false) {
      // Generate masterId
    } else {
      p.masterId = null;
    }

    if (
      Object.prototype.hasOwnProperty.call(options, 'liveStreamOpenHouse') &&
      options.liveStreamOpenHouse !== false
    ) {
      // Generate liveStreamOpenHouse
    } else {
      p.liveStreamOpenHouse = null;
    }

    if (Object.prototype.hasOwnProperty.call(options, 'realogy') && options.realogy !== false) {
      // Generate liveStreamOpenHouse
    } else {
      p.realogy = null;
    }

    return p;
  },
  GeneratePropertyListing(options = []) {
    if (options) {
      /* config holder */
    }
  },
  GenerateListing(options = []) {
    if (options) {
      /* config holder */
    }

    if (options) {
      /* config holder */
    }

    const price = faker.datatype.number({ min: 1000, max: 100000000 });

    const agent = faker.helpers.createCard();
    const office = faker.helpers.createCard();

    const AgentSplit = faker.datatype.number(1, 99);
    const AgentSplit2 = 100 - AgentSplit;

    const ID = UUID();

    return lMerge(lClone(ListingDTO), {
      listingId: ID,
      // sourceSystemKey: faker.lorem.slug + '_' + Math.floor(Math.random() * 9000000) + 355,
      sourceSystemKey: lSample(GeneratorOptions.sourceSystemKey),
      sourceSystemName: lSample(GeneratorOptions.sourceSystemKey),
      standardStatus: lSample(GeneratorOptions.standardStatus),
      buildingPermits: lSample(GeneratorOptions.buildingPermits),
      documentsAvailable: lSample(GeneratorOptions.documentsAvailable),
      disclosures: lSample(GeneratorOptions.disclosures),
      contract: {
        currentFinancing: lSample(GeneratorOptions.currentFinancing),
        specialListingConditions: {
          isForeclosure: faker.datatype.boolean(),
          isShortSale: faker.datatype.boolean(),
          isProbateSale: faker.datatype.boolean(),
        },
      },
      price: {
        listPrice: price,
        listPriceHigh: faker.datatype.number(
          price + faker.datatype.number({ min: 55000, max: 1555999999 }),
        ),
        isPriceReduced: faker.datatype.boolean(),
        priceChangeTimestamp: faker.datatype.boolean() ? faker.date.past(2, '2021-09-29') : '',
        originalListPrice: faker.datatype.number(
          price + faker.datatype.number({ min: 55000, max: 1555999999 }),
        ),
        closePrice: 0,
        petRent: faker.datatype.number({ min: 0, max: 150 }),
        monthsRentUpfront: faker.datatype.number({ min: 0, max: 2 }),
      },
      agentOffice: {
        listAgent: {
          listAgentFullname: agent.name,
          listAgentMlsId: UUID(),
          listAgentOfficePhone: faker.phone.phoneNumber,
          listAgentOfficePhoneType: lSample(GeneratorOptions.phoneType),
          listAgentStateLicense: faker.random.alphaNumeric({ min: 8, max: 32 }),
          listAgentStateLicenseState: faker.address.stateAbbr(),
          listAgentEmail: agent.email,
          listAgentActive: faker.datatype.boolean(),
          listAgentAddress: agent.address.street,
          listAgentCity: agent.address.city,
          listAgentStateOrProvince: faker.address.stateAbbr(),
          listAgentPostalCode: agent.address.zipCode,
          listAgentType: lSample(GeneratorOptions.agentType),
          listAgentOriginalEntryTimestamp: faker.date.past(),
          listAgentModificationTimestamp: faker.date.past(),
        },
        listOffice: {
          listOfficeName: `${faker.company.companyName()} ${faker.company.companySuffix}`,
          listOfficePhone: faker.phone.phoneNumber(),
          listOfficeMlsId: UUID(),
          listOfficeAddress: office.address.street,
          listOfficeCity: office.address.city,
          listOfficeStateOrProvince: faker.address.stateAbbr(),
          listOfficePostalCode: office.address.zipCode,
          listOfficeEmail: office.email,
          listOfficeFax: office.phoneNumber,
          listOfficeOriginalEntryTimestamp: faker.date.past(),
          listOfficeModificationTimestamp: faker.date.past(),
        },
        // We need to handle the co-agents...
        coListAgent: null,
        coListOffice: null,
        buyerAgent: null,
        buyerOffice: null,
        coBuyerAgent: null,
        coBuyerOffice: null,
      },
      compensation: {
        listAgencyCompensation: {
          percentage: AgentSplit,
          fee: faker.datatype.number({ min: 500, max: 5000 }),
        },
        buyerAgencyCompensation: {
          percentage: AgentSplit2,
          fee: faker.datatype.number({ min: 500, max: 5000 }),
        },
      },
      dates: {
        listingContractDate: faker.datatype.boolean ? faker.date.past() : '',
        firstAppearedDate: faker.date.past(),
        expirationDate: faker.datatype.boolean ? faker.date.future() : '',
        lastChangeDate: Math.random() >= 0.7 ? faker.date.past() : '',
        statusChangeDate: faker.date.past(),
        insertedDate: faker.date.past(),
        yearConverted: faker.datatype.number(1984, 2021),
        age: faker.datatype.number({ min: 0, max: 2 }),
        originalEntryTimestamp: faker.date.past(),
        closeDate: faker.datatype.boolean ? faker.date.future() : '',
        cancellationDate: faker.datatype.boolean ? faker.date.future() : '',
        pendingTimestamp: faker.datatype.boolean ? faker.date.future() : '',
        onMarketDate: faker.date.past(),
        contingentDate: faker.datatype.boolean ? faker.date.future() : '',
        offMarketDate: faker.datatype.boolean ? faker.date.future() : '',
        cumulativeDaysOnMarket: faker.datatype.number(3, 300),
        modificationTimestamp: faker.date.past(),
      },
      remarks: {
        publicRemarks: faker.lorem.sentence,
        privateRemarks: faker.datatype.boolean ? faker.lorem.sentence() : '',
        miscInfo: faker.datatype.boolean ? faker.lorem.sentence() : '',
        sellingComments: Math.random() >= 0.3 ? faker.lorem.sentence() : '',
      },
      marketing: {
        virtualTourUrlUnbranded: faker.internet.url(),
        internetAutomatedValuationDisplay: faker.datatype.boolean(),
        internetConsumerComment: faker.datatype.boolean(),
        internetEntireListingDisplay: faker.datatype.boolean(),
        internetAddressDisplay: faker.datatype.boolean(),
        websiteDetailPageUrl: faker.internet.url(),
      },
      closing: {
        availabilityDate: faker.datatype.boolean ? faker.date.future() : '',
      },
      homeWarranty: faker.datatype.boolean(),
      mlsStatus: lSample(GeneratorOptions.mlsStatus),
      pendingOffer: faker.datatype.boolean(),
      daysOnMarket: faker.datatype.number({ min: 0, max: 365 }),
      rdmSourceSystemKey: lSample(GeneratorOptions.rdmSourceSystemKey),
    });
  },
  GenerateLocation(options = []) {
    if (options) {
      /* config holder */
    }

    const card = faker.helpers.createCard();
    const StreetNum = faker.datatype.number(1000, 99999);

    return lMerge(lClone(LocationDTO), {
      gis: {
        crossStreet: faker.datatype.boolean ? card.address.street : '',
        mapCoordinate: lSample(GeneratorOptions.mapCoordinate),
        directions: faker.datatype.boolean ? faker.lorem.sentence() : '',
        latitude: faker.address.latitude(),
        longitude: faker.address.longitude(),
        mlsLatitude: faker.address.latitude(),
        mlsLongitude: faker.address.longitude(),
        parcelLatitude: faker.address.latitude(),
        parcelLongitude: faker.address.longitude(),
        geocodedCity: card.address.city,
        neighborhoodId: lSample(GeneratorOptions.neighborhoodId),
      },
      address: {
        unparsedAddress: `${StreetNum} ${card.address.street}`,
        city: card.address.city,
        countyOrParish: faker.address.county(),
        stateOrProvince: faker.address.stateAbbr(),
        postalCode: faker.address.zipCode(),
        streetDirPrefix: lSample(GeneratorOptions.streetDirPrefix),
        streetDirSuffix: lSample(GeneratorOptions.streetDirSuffix),
        streetName: card.address.street,
        streetNumber: StreetNum,
        unitNumber: faker.datatype.boolean ? faker.datatype.number(1, 42000) : '',
        township: faker.datatype.boolean ? card.address.city : '',
      },
      area: {
        mlsAreaMajor: lSample(GeneratorOptions.mlsAreaMajor),
        subdivisionName: lSample(GeneratorOptions.subdivisionName),
      },
      school: {
        schoolDistrict: `${card.name} District`,
        elementarySchool: `${card.name} Elementary`,
        elementarySchoolDistrict: `${card.name} District`,
        middleOrJuniorSchool: `${faker.name.findName()} Middle School`,
        highSchool: `${faker.name.findName()} High School`,
        highSchoolDistrict: `${card.name} District`,
        middleOrJuniorSchoolDistrict: `${card.name} District`,
        sdUnifId: faker.datatype.number(10000, 9999999),
        sdElemId: faker.datatype.number(10000, 9999999),
        sdSecId: faker.datatype.number(10000, 9999999),
        sazElemId: faker.datatype.number(10000, 9999999),
        sazMiddleId: faker.datatype.number(10000, 9999999),
        sazHighId: faker.datatype.number(10000, 9999999),
      },
    });
  },
  GenerateStructure() {
    return lMerge(lClone(StructureDTO), {
      architectureStyle: lSample(GeneratorOptions.architectureStyle),
      heating: lSample(GeneratorOptions.heating),
      cooling: lSample(GeneratorOptions.cooling),
      constructionMaterials: lSample(GeneratorOptions.constructionMaterials),
      flooring: lSample(GeneratorOptions.flooring),
      levels: lSample(GeneratorOptions.levels),
      bedroomsTotal: faker.datatype.number({ min: 0, max: 5 }),
      bathroomsFull: faker.datatype.number({ min: 0, max: 5 }),
      bathroomsPartial: faker.datatype.number({ min: 0, max: 3 }),
      bathroomsOnequarter: faker.datatype.number({ min: 0, max: 2 }),
      bathroomsThreequarter: faker.datatype.number({ min: 0, max: 2 }),
      bathroomsHalf: faker.datatype.number({ min: 0, max: 2 }),
      buildingName: lSample(GeneratorOptions.buildingName),
      buildingFeatures: lSample(GeneratorOptions.buildingFeatures),
      buildingAreaTotal: faker.datatype.number(500, 7500),
      buildingAreaSource: lSample(GeneratorOptions.buildingAreaSource),
      livingArea: faker.datatype.number(300, 6600),
      roof: lSample(GeneratorOptions.roof),
      parkingFeatures: lSample(GeneratorOptions.parkingFeatures),
      parkingTotal: faker.datatype.number({ min: 0, max: 5 }),
      otherParking: lSample(GeneratorOptions.otherParking),
      otherParkingSpaces: lSample(GeneratorOptions.otherParkingSpaces),
      garageSpaces: faker.datatype.number({ min: 0, max: 5 }),
      carportSpaces: faker.datatype.number({ min: 0, max: 3 }),
      coveredSpaces: faker.datatype.number({ min: 0, max: 3 }),
      openParkingSpaces: faker.datatype.number({ min: 0, max: 3 }),
      yearBuilt: faker.datatype.number({ min: 1880, max: 2023 }),
      storiesTotal: faker.datatype.number({ min: 1, max: 4 }),
      fireplace: faker.datatype.boolean(),
      fireplaceFeatures: lSample(GeneratorOptions.fireplaceFeatures),
      fireplaceTotal: faker.datatype.number({ min: 0, max: 5 }),
      doorFeatures: lSample(GeneratorOptions.doorFeatures),
      foundationDetails: lSample(GeneratorOptions.foundationDetails),
      insulationDesc: lSample(GeneratorOptions.insulationDesc),
      roomType: lSample(GeneratorOptions.roomType),
      builderName: lSample(GeneratorOptions.builderName),
      accessibilityFeatures: lSample(GeneratorOptions.accessibilityFeatures),
      floodArea: lSample(GeneratorOptions.floodArea),
      homeownersProtPlan: lSample(GeneratorOptions.homeownersProtPlan),
      exteriorFeatures: lSample(GeneratorOptions.exteriorFeatures),
      interiorFeatures: lSample(GeneratorOptions.interiorFeatures),
      bodyType: lSample(GeneratorOptions.bodyType),
      basement: lSample(GeneratorOptions.basement),
      otherStructures: lSample(GeneratorOptions.otherStructures),
      builderModel: lSample(GeneratorOptions.builderModel),
      structureType: lSample(GeneratorOptions.structureType),
      rooms: {
        roomsTotal: faker.datatype.number({ min: 0, max: 5 }),
        kitchenDim: '',
        livingRmDim: '',
        masterBrDim: '',
        diningRmDim: '',
        familyRmDim: '',
        diningDesc: lSample(GeneratorOptions.diningDesc),
        familyRoomDesc: lSample(GeneratorOptions.familyRoomDesc),
        kitchenDesc: lSample(GeneratorOptions.kitchenDesc),
        livingRmDesc: lSample(GeneratorOptions.livingRmDesc),
        bedroomDesc: lSample(GeneratorOptions.bedroomDesc),
        bathroomDesc: lSample(GeneratorOptions.bathroomDesc),
      },
      propertyCondition: {
        isFixerUpper: faker.datatype.boolean(),
        isNewConstruction: faker.datatype.boolean(),
      },
      approximateOfficeSquarefeet: faker.datatype.number({ min: 0, max: 1500 }),
      approximateRetailSquarefeet:
        Math.random() >= 0.8 ? faker.datatype.number({ min: 0, max: 5200 }) : 0,
      approximateWarehouseSquarefeet:
        Math.random() >= 0.8 ? faker.datatype.number({ min: 0, max: 5200 }) : 0,
      totalRestrooms: Math.random() >= 0.8 ? faker.datatype.number({ min: 0, max: 15 }) : 0,
      load: lSample(GeneratorOptions.load),
    });
  },
  GenerateMedia(options = []) {
    if (options) {
      /* config holder */
    }

    const mediaInfo = [];
    const d = new Date();
    const ti = faker.datatype.number({ min: 0, max: 64 });

    for (let i = 0; i < ti; i += 1) {
      const imageURL = `${faker.image.city()}?random=${Date.now()}`;
      mediaInfo.push(
        lMerge(lClone(MediaInfoDTO), {
          indexNum: i,
          mediaUrl: imageURL,
          photosChangeTimestamp: d,
          imageHeight: faker.datatype.number({ min: 0, max: 1920 }),
          imageWidth: faker.datatype.number({ min: 0, max: 1080 }),
          md5: crypto.createHash('md5').update(imageURL).digest('hex'),
        }),
      );
    }

    return {
      numImages: ti,
      modificationTimestamp: d,
      imageHashCode: crypto.createHash('md5').update(mediaInfo.toString()).digest('hex'),
      uuid: UUID(),
      lastChangeTimestamp: d,
      mediaInfo,
    };
  },
  GenerateCharacteristics(options = []) {
    if (options) {
      /* config holder */
    }

    return lMerge(lClone(CharacteristicsDTO), {
      lotSizeAcres: `${faker.datatype.number({ min: 0.05, max: 500000 })}`,
      lotSizeDimensions: `${faker.datatype.number({
        min: 0.05,
        max: 50000,
      })}x${faker.datatype.number({ min: 0.05, max: 50000 })}`,
      lotFeatures: lSample(GeneratorOptions.lotFeatures),
      lotSizeSquareFeet: faker.datatype.number({ min: 0.05, max: 50000 }),
      poolFeatures: lSample(GeneratorOptions.poolFeatures),
      privatePool: faker.datatype.boolean(),
      view: lSample(GeneratorOptions.view),
      laundryFeatures: lSample(GeneratorOptions.laundryFeatures),
      spaFeatures: lSample(GeneratorOptions.spaFeatures),
      communityFeatures: lSample(GeneratorOptions.communityFeatures),
      complexName: lSample(GeneratorOptions.complexName),
      numberOfUnitsInCommunity: faker.datatype.number({ min: 2, max: 1000 }),
      waterBodyName: lSample(GeneratorOptions.waterBodyName),
      waterFrontFeatures: lSample(GeneratorOptions.waterFrontFeatures),
      waterFront: faker.datatype.boolean(),
      frontageType: lSample(GeneratorOptions.frontageType),
      numberOfUnitsTotal: faker.datatype.number({ min: 1, max: 500 }),
      hideFromPreloginSearch: faker.datatype.boolean(),
      seniorCommunity: faker.datatype.boolean(),
      isSmartHome: faker.datatype.boolean(),
      currentUse: lSample(GeneratorOptions.currentUse),
      possibleUse: lSample(GeneratorOptions.possibleUse),
      numberOfLots: faker.datatype.number({ min: 0, max: 1000 }),
      numberOfPads: faker.datatype.number({ min: 0, max: 1000 }),
      developmentStatus: lSample(GeneratorOptions.developmentStatus),
      fencing: lSample(GeneratorOptions.fencing),
      roadSurfaceType: lSample(GeneratorOptions.roadSurfaceType),
      roadResponsibility: lSample(GeneratorOptions.roadResponsibility),
      miscUtilitiesDesc: lSample(GeneratorOptions.miscUtilitiesDesc),
      furnished: lSample(GeneratorOptions.furnished),
      leaseterm: lSample(GeneratorOptions.leaseterm),
      isRentersInsuranceRequired: faker.datatype.boolean(),
    });
  },
  GenerateUtilities(options = []) {
    if (options) {
      /* config holder */
    }

    return lMerge(lClone(UtilitiesDTO), {
      waterSource: lSample(GeneratorOptions.waterSource),
      sewer: lSample(GeneratorOptions.sewer),
      utilities: lSample(GeneratorOptions.utilities),
      numberOfSeparateElectricmeters: faker.datatype.number({ min: 0, max: 100 }),
      numberOfSeparateGasmeters: faker.datatype.number({ min: 0, max: 100 }),
      numberOfSeparateWatermeters: faker.datatype.number({ min: 0, max: 100 }),
    });
  },
  GenerateEquipment(options = []) {
    if (options) {
      /* config holder */
    }

    return lMerge(lClone(EquipmentDTO), {
      otherEquipment: lSample(GeneratorOptions.otherEquipment),
      appliances: lSample(GeneratorOptions.appliances),
      securityFeatures: lSample(GeneratorOptions.securityFeatures),
    });
  },
};
