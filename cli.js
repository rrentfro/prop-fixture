const Commands = require('./lib/commands');
const Arg = require('./lib/arg');
const Mock = require('./scripts/mock/mock');
const Fixture = require('./scripts/fixture/fixture');

const run = async () => {
  const command = Arg();
  if (command.arg && command.command1.toLowerCase()) {
    switch (command.command1.toLowerCase()) {
      default:
        Commands.InvalidCommand();
        break;
      case 'fixture':
        Fixture();
        break;
      case 'mock':
        Mock.NewMock();
        break;
    }
  } else {
    Commands.MOTD();
    Commands.Help.main();
  }

  // const credentials = await Commands.Setup.runSetup();
  // console.log(credentials);
};

run().then();
