const FILTER_TYPE = {
  RADIUS: 'RADIUS',
  PRICE_RANGE: 'PRICE_RANGE',
};

const PROPERTY_OPTIONS = {
  SCHOOLS: 'SCHOOLS',
  SALE_HISTORY: 'SALE_HISTORY',
  TAX_HISTORY: 'TAX_HISTORY',
  MORTGAGE: 'MORTGAGE',
  WALK_SCORE: 'WALK_SCORE',
  FLOOD_ZONE: 'FLOOD_ZONE',
  SOUND: 'SOUND',
  AIR_QUALITY: 'AIR_QUALITY',
  AUTOMATED_VALUATION: 'AUTOMATED_VALUATION',
};

module.exports = {
  FILTER_TYPE,
  PROPERTY_OPTIONS,
};
