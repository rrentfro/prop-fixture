const SearchConstants = require('../../constants/search');
const SearchDTO = require('../../dto/search/search');
const NewPager = require('../../lib/factory/CFPager');

module.exports = {
  SearchListing(DTO) {
    return {
      search: {
        ...DTO,
      },
    };
  },
  ...SearchConstants,
  ...SearchDTO,
  NewPager,
};
