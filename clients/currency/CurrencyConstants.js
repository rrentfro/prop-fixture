const CURRENCY_CODE = {
  USD: 'USD',
  EUR: 'EUR',
  CAN: 'CAN',
  MEX: 'MEX',
  JPN: 'JPN',
  BTC: 'BTC',
  ETH: 'ETH',
};

module.exports = {
  CURRENCY_CODE,
};
