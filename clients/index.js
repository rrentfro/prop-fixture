const account = require('./account/account');
const analytics = require('./analytics/analytics');
const blog = require('./blog/blog');
const calendar = require('./calendar/calendar');
const career = require('./career/career');
const communication = require('./communication/communication');
const content = require('./content/content');
const currency = require('./currency/currency');
const favorite = require('./favorite/favorite');
const lead = require('./lead/lead');
const location = require('./location/location');
const media = require('./media/media');
const people = require('./people/people');
const property = require('./property/property');
const search = require('./search/search');

const clients = {
  account,
  analytics,
  blog,
  calendar,
  career,
  communication,
  content,
  currency,
  favorite,
  lead,
  location,
  media,
  people,
  property,
  search,
};

module.exports = {
  NewClient(config) {
    return {
      config,
      ...clients,
    };
  },
};
