const NewSearchTermDTO = (term, type = 'free', options) => ({
  term,
  type,
  options,
});

const NewSortDTO = (order, field, sort) => ({
  order,
  field,
  sort,
});

const NewFilterDTO = (order, type, field, value, op) => ({
  order,
  type,
  field,
  value,
  op,
});

const NewSearchDTO = (SearchTermDTO, SortList, FilterList, CFPager) => ({
  type: 'SearchQuery',
  target: 'search',
  action: 'listing',
  term: SearchTermDTO.term,
  op: SearchTermDTO.type,
  options: SearchTermDTO.options,
  filter: FilterList,
  sort: SortList,
  paging: CFPager,
});

module.exports = {
  NewSearchTermDTO,
  NewSortDTO,
  NewFilterDTO,
  NewSearchDTO,
};
