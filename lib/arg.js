module.exports = () => {
  if (process.argv && process.argv.length > 2) {
    return {
      arg: true,
      command1: process.argv.length >= 3 ? process.argv[2] : '',
      command2: process.argv.length >= 4 ? process.argv[3] : '',
      command3: process.argv.length >= 5 ? process.argv[4] : '',
    };
  }
  return {
    arg: false,
  };
};
