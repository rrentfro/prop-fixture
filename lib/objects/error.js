const lClone = require('lodash/clone');

module.exports = (error = false, errors = []) => lClone({
  error,
  errors,
});
