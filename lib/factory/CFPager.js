module.exports = (page, perPage, totalItems = null, totalPages = null) => ({
  page,
  perPage,
  totalItems,
  totalPages,
});
