const lMerge = require('lodash/merge');
const lClone = require('lodash/clone');
const Common = require('../../dto/common/common');

module.exports = {
  Combine(DTO, search, sort, filter, page) {
    if (DTO && typeof DTO === 'object') {
      let r = lMerge(lClone(Common), lClone(DTO));

      if (search && typeof search === 'object') {
        r = lMerge(r, search);
      }

      if (sort && typeof sort === 'object') {
        r = lMerge(r, sort);
      }

      if (filter && typeof filter === 'object') {
        r = lMerge(r, filter);
      }

      if (page && typeof page === 'object') {
        r = lMerge(r, page);
      }

      return lClone(r);
    }

    return new Error('Invalid DTO');
  },
};
