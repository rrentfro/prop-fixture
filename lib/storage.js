const Configstore = require('configstore');

const conf = new Configstore('RealLaunchSDK');

module.exports = {
  write(key, value) {
    conf.set(key, value);
  },
  read(key) {
    conf.get(key);
  },
};
