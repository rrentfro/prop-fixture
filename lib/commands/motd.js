const chalk = require('chalk');
const figlet = require('figlet');
const PackageVersion = require('../../package').version;
const ascii = require('../constant/ascii');

module.exports = () => {
  console.log(
    chalk.greenBright(
      figlet.textSync('Realogy Core Fabric', {
        horizontalLayout: 'full',
        font: 'stick letters',
      }),
    ),
  );

  console.log(chalk.greenBright(ascii.line));
  console.log(chalk.blueBright(`Version: ${PackageVersion}`));
  console.log(chalk.greenBright(ascii.split2));
};
