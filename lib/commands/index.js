const MOTD = require('./motd');
const Setup = require('./setup');
const Help = require('./help');
const InvalidCommand = require('./InvalidCommand');

module.exports = {
  MOTD,
  Setup,
  Help,
  InvalidCommand,
};
