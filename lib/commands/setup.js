const inquirer = require('inquirer');

module.exports = {
  runSetup: () => {
    const questions = [
      {
        name: 'setup-action',
        type: 'list',
        message: 'What would you like to setup',
        choices: ['Choice 1', 'Choice 2', 'Choice 3'],
        validate(value) {
          if (value.length) {
            return true;
          }
          return 'Please enter your username or e-mail address.';
        },
      },
      {
        name: 'password',
        type: 'password',
        message: 'Enter your password:',
        validate(value) {
          if (value.length) {
            return true;
          }
          return 'Please enter your password.';
        },
      },
    ];
    return inquirer.prompt(questions);
  },
};
