const chalk = require('chalk');

const CommandList = [
  // {
  //     name: 'init',
  //     desc: 'Initializes a new project, sets up deployment resources in the cloud, and makes your project ready for Realogy'
  // },
  // {
  //     name: 'config',
  //     desc: 'Configures the attributes of your project'
  // },
  // {
  //     name: 'build',
  //     desc: 'Compiles the Realogy Launch client for your settings'
  // },
  // {
  //     name: 'status',
  //     desc: 'Shows the state of local resources and configurations'
  // },
  // {
  //     name: '<category> add',
  //     desc: 'Adds a resource for a Realogy category in your local backend'
  // },
  // {
  //     name: '<category> update',
  //     desc: 'Updates a resource for a Realogy category in your local backend'
  // },
  // {
  //     name: '<category> delete',
  //     desc: 'Deletes a resource for a Realogy category in your local backend'
  // },
  // {
  //     name: '<category>',
  //     desc: 'Displays subcommands of the specified Realogy category'
  // },
  // {
  //     name: 'list',
  //     desc: 'Lists all the available categories'
  // },
  {
    name: 'fixture',
    desc: 'Generate fixture data',
  },
  {
    name: 'mock',
    desc: 'Brings mock backend online for testing',
  },
  // {
  //     name: 'env',
  //     desc: 'Displays and manages environment related information for your Realogy project'
  // },
  // {
  //     name: 'console',
  //     desc: 'Opens the web console for the Realogy Launch compiler'
  // },
];

module.exports = () => {
  CommandList.forEach((i) => {
    console.log(chalk.blueBright(`${i.name} `) + chalk.greenBright(`- ${i.desc}`));
  });
};
