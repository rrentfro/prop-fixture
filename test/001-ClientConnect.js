// Core Fabric API Client
const CoreFabricAPI = require('../clients');

// Create a new client
const Realogy = CoreFabricAPI.NewClient({
  ClientID: 'BRAND-SITE-X',
  ClientVersion: '09-29-2021',
  Service: 'CFS',
  UserToken: 'OKTA_TOKEN_HERE',
});

// Create a search term and options
const SearchTermDTO = Realogy.search.NewSearchTermDTO('123 Test Dr.', 'profile', [
  Realogy.search.PROPERTY_OPTIONS.SCHOOLS,
  Realogy.search.PROPERTY_OPTIONS.TAX_HISTORY,
  Realogy.search.PROPERTY_OPTIONS.AUTOMATED_VALUATION,
]);

// Create a Sort list
const SortDTOList = [];
SortDTOList.push(Realogy.search.NewSortDTO(1, 'Property.Price', 'DESC'));
SortDTOList.push(Realogy.search.NewSortDTO(2, 'Property.Geo', 'ASC'));

// Create a New Filter list
const FilterDTOList = [];
FilterDTOList.push(
  Realogy.search.NewFilterDTO(1, Realogy.search.FILTER_TYPE.PRICE_RANGE, 'Property.Price', {
    min: 100000,
    max: 550000,
  }),
);

FilterDTOList.push(
  Realogy.search.NewFilterDTO(2, Realogy.search.FILTER_TYPE.RADIUS, 'Property.Geo', {
    latitude: 2.2515,
    longitude: -1.2535,
    distance: 100,
  }),
);

// Create a New Pager
const pager = Realogy.search.NewPager(1, 50);

// Execute a search for listing with a new search DTO combining all of the above
const query = Realogy.search.SearchListing(
  Realogy.search.NewSearchDTO(SearchTermDTO, SortDTOList, FilterDTOList, pager),
);

// Results
console.log(JSON.stringify(query, null, 2));
